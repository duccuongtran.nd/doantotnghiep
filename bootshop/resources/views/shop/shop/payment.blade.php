@extends('templates.shop.master')
@section('main-content')
{{-- <div id="mainBody"> --}}
	@if(Cart::count() == 0)
		<script type="text/javascript">
			alert('Giỏ hàng rỗng');
			location.href = '../shop';
		</script>

	@endif
	<div class="container">
	<div class="row">
@include ('templates.shop.sidebar')
	<div class="span9">
	@if(Session::has('msg'))
		<script type="text/javascript">
			alert("{{ Session::get('msg')}}");
		</script>
	@endif
	<div class="well">
		<form method="post" action="{{route('shop.shop.payment')}}" class="form-horizontal" >
			{{csrf_field()}}
			<h4>Chọn hình thức thanh toán</h4>	
			<div class="control-group">
				<div class="controls">
				<label>Mã giảm giá</label>
				 <input type="text" name="coupon" id="coupon">
				 <a href="javascript:;" name="btncoupon" id="btncoupon" class="btn">Áp dụng</a>
			</div>  
			<div id="checkpay"> </div>
			<div class="control-group">
				<div class="controls">
				 <input type="radio" checked="checked" name="payment" value="1"> Thanh toán sau khi nhận hàng <br>
				 	<img src="{{$ImagesPath}}/tttn.png" width="70px">
					 <div id="paypal-button-container">
	  				 	<input id="forwardStep" type="radio" name="payment" value="2"> Thanh toán với Paypal 
	  				 </div>
  				 </div>
			</div>
			
			<div class="control-group">
				<div class="controls">	
					<input class="btn btn-large btn-success" type="submit" value="Tiếp tục" />
				</div>
			</div>		
		</form>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#forwardStep').bind('click',function(){
				  var page = $(this).attr('rel');
				  	alert('Kích vào nút Paypal để tiến hành thanh toán');
				  	alert('Trong trường hợp Paypal chưa nhận được giá tiền sau khi giảm giá, Bạn vui lòng tải lại trang web !');
				 });
			});
		</script>
	</div>
	<div class="well" id="cp">
		<table style="color:green;" class="table table-bordered">
			<th>Tên sản phẩm</th>
			<th>Số lượng</th>
			<th>Giá</th>
			@foreach($checkCart as $arCheckCart)
				@php
            		$slug = str_slug($arCheckCart->name);
        			$url = route('shop.shop.detail',['slug'=>$slug,'id'=>$arCheckCart->id]);
            	@endphp
			<tr>
				<td>
					<a href="{{$url}}" style="color:red">{{$arCheckCart->name}}</a>
				</td>
				<td>
					{{$arCheckCart->qty}}
				</td>
				<td>
					{{$arCheckCart->price}}.000 VNĐ
				</td>
			</tr>
			@endforeach
			<tr>
				<td  colspan="6" style="text-align:right">Tổng tiền : {{Cart::subtotal()}}.000 VNĐ</td>
			</tr>
			@if(Session::has('coupon'))
				<tr>
					<td  colspan="6" style="text-align:right">Giảm còn : {!!  Cart::subtotal(0,'','')*Session::get('coupon')!!}.000 VNĐ</td>
				</tr>
			@endif
		</table>
	</div>
	@php
		if (Session::has('coupon')) {
			$getCoupon = Session::get('coupon');
			$subtt = Cart::subtotal(0,'','');
			$tongtien = $subtt * $getCoupon;
		}else{
			$tongtien = Cart::subtotal();
		}
	@endphp
	<script>
        paypal.Button.render({

            env: 'sandbox', // sandbox | production

            // PayPal Client IDs - replace with your own
            // Create a PayPal app: https://developer.paypal.com/developer/applications/create
            client: {
                sandbox:    'AYJV6wajsx2bceb_ApGHfR0kA3Bdy5NwdIfIUCk6D3YkXT8cVaTQ86ZAJ1M60N02VTt-q6scbsyApoKW',
                production: '<insert production client id>'
            },

            // Show the buyer a 'Pay Now' button in the checkout flow
            commit: true,

            // payment() is called when the button is clicked
            payment: function(data, actions) {

                // Make a call to the REST api to create the payment
                return actions.payment.create({
                    payment: {
                        transactions: [
                            {
                                amount: { total: '{{$tongtien}}', currency: 'USD' }
                            }
                        ]
                    }
                });
            },

            // onAuthorize() is called when the buyer approves the payment
            onAuthorize: function(data, actions) {

                // Make a call to the REST api to execute the payment
                return actions.payment.execute().then(function() {
                	document.getElementById("forwardStep").click();
                    document.getElementById("checkpay").innerHTML = "<input type='hidden' name='check' value='ok'>";
                    window.alert('Thanh toán thành công!');
                    

                });
            }

        }, '#paypal-button-container');
    </script>

</div>
</div>
</div>
@stop