<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VCustomer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname' => 'required|min:10|max:50',
            'phone' => 'required|numeric',
            
            'address' => 'required|min:10',
        ];
    }
    public function messages()
    {
        return [
            'fullname.required' => 'Vui lòng điền vào trường này', 
            'fullname.min' => 'Họ và tên nên có ít nhất 10 kí tự',
            'fullname.max' => 'Họ và tên không vượt quá 50 kí tự',
            'phone.required' => 'Vui lòng điền vào trường này',
            'phone.numeric' => 'Điện thoại phải là số',

            
            'address.required' => 'Vui lòng điền vào trường này',
            'address.min' => 'Địa chỉ nên có ít nhất 10 kí tự',
        ];
    }
}
