<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="bootshop - vne">
    <meta name="author" content="Nguyễn Chí Công">
    <title>Login Page</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ $adminUrl }}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ $adminUrl }}/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ $adminUrl }}/dist/css/sb-admin-2.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="{{ $adminUrl }}/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="{{ $adminUrl }}/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ $adminUrl }}/dist/css/sb-admin-2.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="{{ $adminUrl }}/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ $adminUrl }}/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
            <div class="container">
    <div class="row">
        <button class="btn btn-danger btn-lg btn-block"><span class="glyphicon glyphicon-user"></span> LOGIN </button>
    </div>
    @if(Session::has('msg'))
        <script> alert('{{ Session::get('msg') }}')</script>
    @endif
    <div class="row">
        <img src="http://www.logogarden.com/user_images/logo_1613886_iwq4dsj66_screen_display.png" class="chatup_logo center-block" />
    </div>
    <form method="post" action="{{route('auth.auth.login')}}">
        {{csrf_field()}}
    <div class="form-group">
        <label for="username">Username</label>
        <input type="text" required="required" placeholder="" class="form-control" name="username" placeholder="Your username" />
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" required="required" placeholder="" class="form-control" name="password" placeholder="Your password" />
    </div>
    <div class="btn-group-vertical btn-block text-center" role="group">
        <button class="btn btn-danger btn-lg">LOGIN</button>
    </div>
    </form>
            <!-- /.row -->
        <!-- /#page-wrapper -->
</div>
</html>