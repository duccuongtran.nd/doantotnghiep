@extends('templates.admin.master')
@section('main-content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Quản lý liên hệ</h1>
                </div>
                <!-- /.col-lg-12 -->
                @if(Session::has('msg'))
                    <script> alert('{{ Session::get('msg') }}')</script>
                @endif
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th><center>Fullname</center></th>
                                        <th><center>Email</center></th>
                                        <th><center>Tiêu đề</center></th>
                                        <th><center>Thông điệp</center></th>
                                        <th><center>Trạng thái</center></th>
                                        <th><center>Ngày liên hệ</center></th>
                                        <th><center>Hành động</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($objContact as $arContact)
                                    @php
                                        $message = $arContact['message'];
                                        $cutMessage = str_limit($message,40,'...');
                                        $status = $arContact['status'];
                                        if ($status == 1) {
                                            $tt = 'Đã xem';
                                            $class = 'btn btn-success';
                                        }else{
                                            $tt = 'Chưa xem';
                                            $class = 'btn btn-danger';
                                        }

                                    @endphp
                                    <tr class="odd gradeX">
                                        <td width="5%">{{ $arContact->id }}</td>
                                        <td><center>{{ $arContact->name }}</center></td>
                                        <td><center>{{ $arContact->email }}</center></td>
                                        <td><center>{{ $arContact->subject}}</center></td>
                                        <td><center>{{ $cutMessage}}</center></td>
                                        <td><a href="" class="{{ $class }}" >{{ $tt }}</a></td>
                                        <td><center>{{ $arContact->created_at }}</center></td>
                                        <td width="25%">
                                            <a href="{{ route('admin.contact.view',$arContact->id)}}" class="btn btn-success"><i class="glyphicon glyphicon-eye-open"></i> Xem</a>
                                            <a href="mailto:{{$arContact->email}}?subject=Tin nhắn phản hồi đến từ Bootshop" class="btn btn-warning"><i class="fa fa-reply"></i> Trả lời</a>
                                            <a href="{{ route('admin.contact.del',$arContact->id)}}" onclick="return confirm('Bạn có muốn xóa liên hệ này?')" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Xóa</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $objContact->links()}}
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        <!-- /#page-wrapper -->
@stop