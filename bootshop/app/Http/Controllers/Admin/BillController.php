<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Bill;
use App\Model\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
class BillController extends Controller
{
    public function index(){
    	$objOrder =
            DB::table('bills')
            /*->join('bill_detail','bill_detail.id_bill','=','bills.id')*/
            ->join('customer','customer.id','=','bills.customer_id')
            ->orderBy('bills.id','DESC')
            ->select('customer.name','customer.email','customer.address','customer.phone','customer.note','bills.total','bills.payment','bills.id as bid','bills.checkpay')
            ->paginate(10);
    	return view('admin.order.index',compact('objOrder'));
    }
    public function getView($id){
    	$objOrder =
            DB::table('bills')
            ->join('bill_detail','bill_detail.id_bill','=','bills.id')
            ->join('customer','customer.id','=','bills.customer_id')
            ->orderBy('bills.id','DESC')
            ->where('bills.id','=',$id)
            ->select('customer.name','customer.email','customer.address','customer.phone','customer.note','bill_detail.quantity','bill_detail.status','bills.total','bills.payment','bills.id as bid','bills.checkpay','bills.created_at as date')
            ->first();
        $objProducts =
            DB::table('bill_detail')
            ->join('products','products.id','=','bill_detail.id_product')
            ->orderBy('products.id','DESC')
            ->where('bill_detail.id_bill','=',$id)
            ->select('bill_detail.quantity','products.name','products.unit_price','products.promotion_price')
            ->get();
     	return view('admin.order.view',compact('objOrder','objProducts'));
    }
    public function postView($id, Request $request){
    	$checkpay = $request->checkpay;
    	$status = $request->status;
    	DB::table('bills')
            ->where('id','=',$id)
            ->update(['checkpay'=>$checkpay]);
        DB::table('bill_detail')
            ->where('id_bill','=',$id)
            ->update(['status'=>$status]);
        $request->session()->flash('msg','Đã lưu');
        return redirect()->route('admin.order.index');
    }
    public function del($id, Request $request){
    	$getBill = Bill::where('id','=',$id)->first();
    	$idCustomer = $getBill->customer_id;
        DB::table('bills')->where('id','=',$id)->delete();
        DB::table('bill_detail')->where('id_bill','=',$id)->delete();
        DB::table('customer')->where('id','=',$idCustomer)->delete();
        $request->session()->flash('msg','Xóa thành công');
        return redirect()->route('admin.order.index');
    }

    public function indexcheck(Request $request){
        $check_customer = Auth::user()->phone;
        $sdt = "$check_customer";
        $email = Auth::user()->email;
        $objOrder =
            DB::table('bills')
            ->join('bill_detail','bill_detail.id_bill','=','bills.id')
            ->join('customer','customer.id','=','bills.customer_id')
            ->orderBy('bills.id','DESC')
            ->where('customer.phone','=',$sdt)
            ->orWhere('customer.email','=',$email)
            ->select('customer.name','customer.email','customer.address','customer.phone','customer.note','bill_detail.quantity','bill_detail.status','bills.total','bills.payment','bills.id as bid','bills.checkpay','bills.created_at as date')
            ->first();
        if (sizeof($objOrder)==0) {
            dd('Bạn không có đơn đặt hàng nào.');
        }
        $id_bill = $objOrder->bid;
        $objProducts =
            DB::table('bill_detail')
            ->join('products','products.id','=','bill_detail.id_product')
            ->orderBy('products.id','DESC')
            ->where('bill_detail.id_bill','=',$id_bill)
            ->select('bill_detail.quantity','products.name','products.unit_price','products.promotion_price')
            ->get();
        return view('admin.checkorder.index',compact('objOrder','objProducts'));
    }
    public function info(Request $request){
        $id = Auth::user()->id;
        $objUser = User::findOrFail($id);
        return view('admin.checkorder.info',compact('objUser'));
    }
    public function postInfo($id,Request $request){
        $objItems= User::findOrFail($id);
        $password = trim($request->password);
        $oldpassword = trim($request->oldpassword);
        $fullname = trim(stripslashes($request->fullname));
        $email = trim(stripslashes($request->email));
        $note = $request->note;
        if ($note == "") {
            $objItems->note = "";
        }else{
            $objItems->note = $note;
        }
        if ($password != "") {
            if (Hash::check($oldpassword, $objItems->password)) {
                $objItems->password = bcrypt($password);
            }else{
                $request->session()->flash('msg','Mật khẩu cũ không đúng');
                return redirect()->route('admin.checkorder.info',$id); 
            }
        }else{
            $password = $objItems->password;

        }
        $objItems->email = $email;
        $objItems->username = $objItems->username;
        $objItems->role = $objItems->role;
        $objItems->phone = $request->phone;
        $objItems->address = $request->address;
        if($objItems->update()){
            $request->session()->flash('msg','Sửa thành công');
            return redirect()->route('admin.checkorder.info');
        }else{
            $request->session()->flash('msg','Đã có lỗi xảy ra!');
            return redirect()->route('admin.checkorder.info');
        }
    }
}
