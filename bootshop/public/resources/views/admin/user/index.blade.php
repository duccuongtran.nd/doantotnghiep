@extends('templates.admin.master')
@section('main-content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Quản lý Users</h1>
                </div>
                <!-- /.col-lg-12 -->
                @if(Session::has('msg'))
                    <script> alert('{{ Session::get('msg') }}')</script>
                @endif
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th><center>Username</center></th>
                                        <th><center>Phone</center></th>
                                        <th><center>Level</center></th>
                                        <th><center>Fullname</center></th>
                                        <th><center>Date created</center></th>
                                        <th><center>Action</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($objUsers as $arUsers)
                                        @php
                                            if ($arUsers->role == 1) {
                                                $level = "Administrator";
                                            }if($arUsers->role == 2){
                                                $level = "Moderator";
                                            }if($arUsers->role == 3){
                                                $level = "Customer";
                                            }
                                        @endphp
                                    <tr class="odd gradeX">
                                        <td width="5%">{{ $arUsers->id }}</td>
                                        <td><center>{{ $arUsers->username }}</center></td>
                                        <td><center>{{ $arUsers->phone }}</center></td>
                                        <td><center>{{ $level }}</center></td>
                                        <td><center>{{ $arUsers->fullname }}</center></td>
                                        <td><center>{{ $arUsers->created_at }}</center></td>
                                        <td width="17%">
                                            <a href="{{ route('admin.user.edit',$arUsers->id)}}" class="btn btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                            <a href="{{ route('admin.user.del',$arUsers->id)}}" onclick="return confirm('Are you sure you want to delete this user?')" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $objUsers->links()}}
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        <!-- /#page-wrapper -->
@stop