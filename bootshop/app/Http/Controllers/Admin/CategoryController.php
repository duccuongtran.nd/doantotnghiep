<?php

namespace App\Http\Controllers\Admin;
use App\Model\Category;
use App\Model\Product;
use App\Http\Requests\CatRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index(){
    	$objCat = Category::paginate(10);
    	return view('admin.category.index',compact('objCat'));
    }
    public function getAdd(){
    	return view('admin.category.add');
    }
    public function postAdd(CatRequest $request){
        $category = $request->category;
        $parent_id = $request->pid;
        $arItem = array(
            'name' => $category,
            'parent_id' => 3,
        );
        if(Category::insert($arItem)){
            $request->session()->flash('msg','Thêm thành công!');
            return redirect()->route('admin.category.index');
        }else{
            $request->session()->flash('msg','Đã có lỗi xảy ra!');
            return redirect()->route('admin.category.add');
        }
    }
    public function getEdit($id){
    	$objCat = Category::findOrFail($id);
      return view('admin.category.edit',compact('objCat'));
    }
    public function postEdit($id,Request $request){
        $objItem= Category::find($id);
        $cat_name = $request->category;
        if ($cat_name == $objItem->name ) {
            $objItem->name = $cat_name;
        }
        $objItem->name = $cat_name;
       
        if($objItem->update()){
            $request->session()->flash('msg','Sửa thành công');
            return redirect()->route('admin.category.index');
        }else{
            $request->session()->flash('msg','Đã có lỗi xảy ra!');
            return redirect()->route('admin.category.index');
        }
    }
    public function del($id, Request $request){
        $objItem = Category::FindOrFail($id);
        $arNew = Product::where('cat_id','=',$id)->get();
        foreach ($arNew as $arNews) {
            $picture = $arNews->image;
            if ($picture != '') {
                Storage::delete('files/'.$picture);
            }
            $arNews->delete();
        }
        if($objItem->delete()){
          $request->session()->flash('msg','Xóa thành công');
          return redirect()->route('admin.category.index');
          }else{
          $request->session()->flash('msg','Đã có lỗi xảy ra!');
          return redirect()->route('admin.category.index');
        }
    }
}
