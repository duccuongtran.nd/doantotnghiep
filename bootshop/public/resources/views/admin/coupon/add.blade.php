@extends('templates.admin.master')
@section('main-content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Thêm mã giảm giá</h1>
                </div>
                @if(Session::has('msg'))
                    <script> alert('{{ Session::get('msg') }}')</script>
                @endif
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('admin.coupon.index')}}" class="fa fa-mail-reply" style="font-size:20px;"> Quay lại</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form  action="{{ route('admin.coupon.add')}}" role="form" method="POST">
                                        {{ csrf_field()}}
                                        <div class="form-group">
                                            <label>Tên mã giảm giá</label>
                                            <input class="form-control" value="{{old('name')}}" name="name">        
                                        </div>
                                        @if ($errors->has('name'))
                                        <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            {{ array_first($errors->get('name')) }}
                                        </div>
                                        @endif
                                        <div class="form-group">
                                            <label>Số lượng</label>
                                            <input class="form-control" value="{{old('st')}}" name="st">        
                                        </div>
                                        @if ($errors->has('st'))
                                        <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            {{ array_first($errors->get('st')) }}
                                        </div>
                                        @endif
                                        <div class="form-group">
                                            <label>Giá trị</label>
                                            <select class="form-control" name="percent">
                                                @php
                                                    for ($i=1; $i < 3; $i++) { 
                                                        if ($i == 1) {
                                                            $giatri = 20;
                                                        }else{
                                                            $giatri = 50;
                                                        }
                                                        echo "<option value='$giatri'>$giatri%</option>";
                                                    }
                                                @endphp
                                            </select>        
                                        </div>
                                        @if ($errors->has('percent'))
                                        <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            {{ array_first($errors->get('percent')) }}
                                        </div>
                                        @endif

                                        <div class="form-group">
                                            <input type="submit" value="Add Payment" class="btn btn-primary">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        <!-- /#page-wrapper -->
@stop