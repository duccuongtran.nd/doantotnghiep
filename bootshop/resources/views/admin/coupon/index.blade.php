@extends('templates.admin.master')
@section('main-content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Quản lý mã giảm giá</h1>
                </div>
                <!-- /.col-lg-12 -->
                @if(Session::has('msg'))
                    <script> alert('{{ Session::get('msg') }}')</script>
                @endif
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('admin.coupon.add')}}" class="fa fa-plus" style="font-size:20px;"> Thêm mã giảm giá</a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" >
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th><center>Tên mã giảm giá</center></th>
                                        <th><center>Số lượng còn lại</center></th>
                                        <th><center>Giá trị</center></th>
                                        <th><center>Action</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($objCoupons as $arCoupon)
                                    <tr class="odd gradeX">
                                        <td width="5%">{{ $arCoupon->id }}</td>
                                        <td><center>{{ $arCoupon->name }}</center></td>
                                        <td><center>{{ $arCoupon->st }}</center></td>
                                        <td><center>{{ $arCoupon->percent }}%</center></td>
                                        <td width="17%">
                                            <a href="{{ route('admin.coupon.edit',$arCoupon->id)}}" class="btn btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                            <a href="{{ route('admin.coupon.del',$arCoupon->id)}}" onclick="return confirm('Xác nhận xóa coupon này?')" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        <!-- /#page-wrapper -->
@stop