<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|min:5|max:100|unique:users,username',
            'fullname' => 'required|min:10|max:100',
            'password' => 'required',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|numeric',
            'address' => 'required|min:10',
        ];
    }
    public function messages()
    {
        return [
            'username.required' => 'Vui lòng điền vào trường này',
            'username.min' => 'Username nên có ít nhất 5 kí tự',
            'username.max' => 'Username không được vượt quá 100 kí tự',
            'username.unique' => 'Username đã tồn tại',
            'password.required' => 'Vui lòng điền vào trường này',
            'email.required' => 'Vui lòng điền vào trường này',
            'email.unique' => 'Email đã có trên hệ thống',
            'fullname.required' => 'Vui lòng điền vào trường này', 
            'fullname.min' => 'Họ và tên nên có ít nhất 10 kí tự',
            'fullname.max' => 'Họ và tên không được vượt quá 100 kí tự',
            'phone.required' => 'Vui lòng điền vào trường này',
            'phone.numeric' => 'Điện thoại phải là số',

            
            'address.required' => 'Vui lòng điền vào trường này',
            'address.min' => 'Địa chỉ nên có ít nhất 10 kí tự',
        ];
    }
}
