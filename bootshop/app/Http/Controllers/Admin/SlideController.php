<?php

namespace App\Http\Controllers\Admin;
use App\Model\Slide;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SlideController extends Controller
{
    public function index(){
    	$objSlide = Slide::paginate(5);
    	return view('admin.slide.index',compact('objSlide'));
    }
    public function getAdd(){
    	return view('admin.slide.add');
    }
    public function postAdd(Request $request){
        $picture = $request->picture;
        $slide = $request->link;
        $arItem = array(
        	'link' => $slide,
        );
        if ($picture != '') {
            $tmp = $request->file('picture')->store('files');
            $pic = explode('/', $tmp);
            $picture = end($pic);
            $arItem['picture'] = $picture;
          }
        if(Slide::insert($arItem)){
            $request->session()->flash('msg','Thêm thành công!');
            return redirect()->route('admin.slide.index');
        }else{
            $request->session()->flash('msg','Đã xảy ra lỗi!');
            return redirect()->route('admin.slide.add');
        }
    }
    public function getEdit($id){
    	$objSlide = Slide::findOrFail($id);
      return view('admin.slide.edit',compact('objSlide'));
    }
    public function postEdit($id,Request $request){
        $objItem= Slide::find($id);
        $objItem->link =$request->link;
        $oldPic = $objItem->picture;
        $picture = $request->picture;
        if ($picture != '') {
            $tmp = $request->file('picture')->store('files');
            $pic = explode('/', $tmp);
            $picture = end($pic);
            // xóa ảnh cũ       
            if ($oldPic != '') {
                Storage::delete('files/'.$oldPic);
            }
            $objItem->picture = $picture;
        }else{
        	$objItem->picture = $oldPic;
        }
        if($objItem->update()){
            $request->session()->flash('msg','Sửa thành công');
            return redirect()->route('admin.slide.index');
        }else{
            $request->session()->flash('msg','Đã xảy ra lỗi!');
            return redirect()->route('admin.slide.index');
        }
    }
    public function del($id, Request $request){
        $objItem = Slide::FindOrFail($id);
        $oldPic = $objItem->picture;
        if ($oldPic != "") {
            Storage::delete('files/'.$oldPic);
        }
        if($objItem->delete()){
          $request->session()->flash('msg','Xóa thành công');
          return redirect()->route('admin.slide.index');
          }else{
          $request->session()->flash('msg','Đã xảy ra lỗi!');
          return redirect()->route('admin.slide.index');
        }
    }
}
