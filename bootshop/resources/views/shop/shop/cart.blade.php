@extends('templates.shop.master')
@section('main-content')
{{-- <div id="mainBody"> --}}
	<div class="container">
	<div class="row">
@include ('templates.shop.sidebar')
	<div class="span9">
    <ul class="breadcrumb">
		<li class="active"> Chi tiết giỏ hàng</li>
    </ul>
	<h3>  GIỎ HÀNG <a href="{{route('shop.shop.shop')}}" class="btn btn-large pull-right"><i class="icon-arrow-left"></i> Quay lại mua sắm </a></h3>	
	<hr class="soft"/>
	@if(Cart::count() == 0)
		<h4 style="color:green"><center>Giỏ hàng rỗng!</center></h4>
	@else
	<table class="table table-bordered" id="show">
              <thead>
                <tr>
		          <th>Tên sản phẩm</th>
		          <th>Số lượng/Cập nhật</th>
		          				  <th>Đơn giá</th>
		          <th>Tổng tiền</th>
				</tr>
              </thead>
              <tbody>
              <form action="" method="GET">
              	<input type="hidden" id="_token" name="_token" value="{{ csrf_token()}}">
              	@foreach($arProducts as $arCart)
              		@php
            			$slug = str_slug($arCart->name);
        				$url = route('shop.shop.detail',['slug'=>$slug,'id'=>$arCart->id]);
            		@endphp
                <tr>
                  <td><a href="{{$url}}">{{ $arCart->name }}</a></td>
				  <td>
					<div class="input-append">
					<input class="span1 qty" style="max-width:34px" placeholder="1" value="{{ $arCart->qty }}" id="appendedInputButtons" size="16" type="text">
					<a class="btn update" id="{{ $arCart->rowId }}" ><i class="icon-refresh"></i></a>
					<a href="{!! route('shop.shop.remove',['id'=>$arCart->rowId])!!}" class="btn btn-danger"><i class="icon-remove icon-white"></i></a>				
					</div>

				  </td>
				  
                  <td>{{ $arCart->price }} .000 VNĐ</td>
                  <td>{{ $arCart->price * $arCart->qty }} .000 VNĐ</td>
                </tr>
				@endforeach
			</form>
                <tr>
                  <td colspan="3" style="text-align:right">Tổng tiền:	</td>
                  <td id="totalc">{{$total}}0 VNĐ</td>
                </tr>
				</tbody>
    </table>
    @endif
            @if(Session::has('msgrm'))
            <div class="alert alert-block alert-error fade in">
			<button type="button" class="close" data-dismiss="alert">×</button>
				{{ Session::get('msgrm')}}
		 	</div>
		 	@endif
            	
	<a href="{{route('shop.shop.shop')}}" class="btn btn-large"><i class="icon-arrow-left"></i> Quay lại mua sắm </a>
	<a href="{{route('shop.shop.step1')}}" class="btn btn-large pull-right">Tiếp tục <i class="icon-arrow-right"></i></a>
	
</div>
</div></div>
@stop