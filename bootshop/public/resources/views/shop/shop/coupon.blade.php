<table style="color:green;" class="table table-bordered">
	<th>Tên sản phẩm</th>
	<th>Số lượng</th>
	<th>Giá</th>
	@foreach($checkCart as $arCheckCart)
		@php
    		$slug = str_slug($arCheckCart->name);
			$url = route('shop.shop.detail',['slug'=>$slug,'id'=>$arCheckCart->id]);
    	@endphp
	<tr>
		<td>
			<a href="{{$url}}" style="color:red">{{$arCheckCart->name}}</a>
		</td>
		<td>
			{{$arCheckCart->qty}}
		</td>
		<td>
			{{$arCheckCart->price}} $
		</td>
	</tr>
	@endforeach
	<tr>
		<td  colspan="6" style="text-align:right">Total : {{Cart::subtotal()}}.000 VNĐ</td>
	</tr>
	<tr>
		<td  colspan="6" style="text-align:right">Giảm còn : {!!Cart::subtotal(0,'','')*Session::get('coupon')!!}0 VNĐ</td>
	</tr>
</table>