$(document).ready(function(){
	$(".addCart").click(function(){
		var id = $(this).attr('id');
		$.ajax({
			url : 'cart/addItem/'+id,
			type : 'GET',
			cache :false,
			data : {"id":id},

			success:function(data){
				/*alert('1 items added into your cart');*/
				$('#qtyCart').empty();
                $('#qtyCart').html(data)
			}
		});
	});
});