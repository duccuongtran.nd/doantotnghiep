@extends('templates.admin.master')
@section('main-content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Product Management</h1>
                </div>
                <!-- /.col-lg-12 -->
                @if(Session::has('msg'))
                    <script> alert('{{ Session::get('msg') }}')</script>
                @endif
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('admin.product.add')}}" class="fa fa-plus" style="font-size:20px;"> Add new products</a>
                        </div>
                        <style type="text/css">
                            .no-sort::after { display: none!important; }
                            .no-sort { pointer-events: none!important; cursor: default!important; }
                        </style>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <form action="{{ route('admin.product.destroy')}}" method="post">
                                    {{ csrf_field()}}
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th class="no-sort"></th>
                                        <th class="no-sort">Product Name</th>
                                        <th class="no-sort">Category</th>
                                        <th>Price</th>
                                        <th class="no-sort">Image</th>
                                        <th class="no-sort">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($objTrash as $arProducts)
                                    <tr class="odd gradeX">
                                        <td width="2%" >
                                            <input type="checkbox" name="delete[]" value="{{ $arProducts->pid }}" />
                                        </td>
                                        <td>{{ $arProducts->p_name }}</td>
                                        <td>{{ $arProducts->cat_name }}</td>
                                        <td>{{ $arProducts->unit_price }}</td>
                                        <td class="center">{{ $arProducts->images }}</td>
                                        <td width="16%">
                                            <a href="{{ route('admin.product.edit',$arProducts->pid)}}" class="btn btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                            <a href="{{ route('admin.product.restore',$arProducts->pid)}}" class="btn btn-success"><i class="fa fa-mail-reply-all"></i> Restore</a>
                                        </td>
                                    </tr>
                                @endforeach  
                                </tbody>
                            </table>
                            </i><input type="submit" value="Delete" class="btn btn-danger"  onclick="return confirm('Are you sure you want to delete this items?')" >
                        </form>       
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        <!-- /#page-wrapper -->
@stop
