@extends('templates.admin.master')
@section('main-content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Quản lý đơn hàng</h1>
                </div>
                <!-- /.col-lg-12 -->
                @if(Session::has('msg'))
                    <script> alert('{{ Session::get('msg') }}')</script>
                @endif
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th><center>ID Bill</center></th>
                                        <th><center>Họ tên</center></th>
                                        <th><center>Phone</center></th>
                                        <th><center>Địa chỉ</center></th>
                                        {{-- <th><center>Status</center></th> --}}
                                        <th><center>Hình thức TT</center></th>
                                        <th><center>Tổng tiền</center></th>
                                        <th><center>Trạng thái</center></th>
                                        <th><center>Actions</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($objOrder as $arOrder)
                                    @php
                                        $bid = $arOrder->bid;
                                        $fullname = $arOrder->name;
                                        $phone = $arOrder->phone;
                                        $email = $arOrder->email;
                                        $address = $arOrder->address;
                                        $checkpay = $arOrder->payment;
                                        $paycheck = $arOrder->checkpay;
                                        //$qty = $arOrder->quantity;
                                        $total = $arOrder->total;
                                        
                                        $cutAddress = str_limit($address,40,'...');
                                        /*$status = $arOrder->status;
                                        if ($status == 1) {
                                            $tt = 'Pending';
                                        }if ($status == 2) {
                                            $tt = 'Shipping';
                                        }if ($status == 3) {
                                            $tt = 'Finished';
                                        }if ($status == 4) {
                                            $tt = 'Rejected';
                                        }
                                        */
                                        if ($checkpay == 1) {
                                            $payment = 'Giao hàng tận nhà';
                                        }elseif($checkpay == 2){
                                            $payment = 'Thông qua Paypal';
                                        }
                                        if ($paycheck == 1) {
                                            $st = 'Đã thanh toán';
                                        }else{
                                            $st = 'Chưa thanh toán';
                                        }

                                    @endphp
                                    <tr class="odd gradeX">
                                        <td>{{$bid}}</td>
                                        <td>{{$fullname}}</td>
                                        <td>{{$phone}}</td>
                                        <td>{{$address}}</td>
                                        {{-- <td>{{$tt}}</td> --}}
                                        <td>{{$payment}}</td>
                                        <td>{{$total}}00 VNĐ</td>
                                        <td>{{$st}}</td>
                                        <td width="18%">
                                            <a href="{{ route('admin.order.view',$bid)}}" class="btn btn-success"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                            <a href="{{ route('admin.order.del',$bid)}}" onclick="return confirm('Are you sure you want to delete this order?')" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete</a>
                                        </td>    
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $objOrder->links()}}
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        <!-- /#page-wrapper -->
@stop