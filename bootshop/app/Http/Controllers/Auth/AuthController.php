<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
class AuthController extends Controller
{
    public function getLogin(){
    	return view('auth.auth.login');
    }
    public function postLogin(Request $request){
    	$username = $request->username;
    	$password = $request->password;
    	if ( Auth::attempt([
    		'username' => $username, 
    		'password' => $password
    	]) ) {
    		//Ok
            /*$arUser = Auth::user();*/
    		return redirect()->route('admin.index.index');
    	}else{
    		$request->session()->flash('msg','Username hoặc password không đúng');
    		return back();
    	}
    }
    public function logout(){
    	Auth::logout();
    	return redirect()->route('auth.auth.login');
    }
}
