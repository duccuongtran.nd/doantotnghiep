@extends('templates.shop.master')
@section('main-content')
<div class="container">
	<hr class="soften">
	@if(Session::has('msg'))
		<center><span class="label label-success">{{ Session::get('msg')}}</span></center>
	@endif
	<h1>Visit us</h1>
	<hr class="soften"/>	
	<div class="row">
		<div class="span4">
		<h4>Chi tiết liên hệ</h4>
		<p>	xx Lê Quang Sung,<br/> DN 550000, Việt Nam
			<br/><br/>
			info@bootshop.com<br/>
			﻿Tel 0169-606-7121<br/>
			Fax 069.-2322347<br/>
			Web:<a href="https://bootshop.com" style="color:purple">bootshop.com</a>
		</p>		
		</div>
			
		<div class="span4">
		<h4>Giờ mở cửa</h4>
			<h5> Thứ 2 - Thứ 6</h5>
			<p>09:00am - 09:00pm<br/><br/></p>
			<h5>Saturday</h5>
			<p>09:00am - 07:00pm<br/><br/></p>
			<h5>Sunday</h5>
			<p>12:30pm - 06:00pm<br/><br/></p>
		</div>
		<div class="span4">
		<h4>Liên hệ </h4>
		<form class="form-horizontal" action="{{route('shop.shop.contact')}}" method="post">
			{{csrf_field()}}
        <fieldset>
          <div class="control-group">
           
              <input type="text" name="name" required="required" placeholder="Họ tên của bạn" class="input-xlarge"/>
           
          </div>
		   <div class="control-group">
           
              <input type="email" name="email" required="required" placeholder="Email của bạn" class="input-xlarge"/>
           
          </div>
		   <div class="control-group">
           
              <input type="text" name="subject" required="required" placeholder="Tiêu đề" class="input-xlarge"/>
          
          </div>
          <div class="control-group">
              <textarea rows="3" name="message" id="textarea" required="required" placeholder="Thông điệp muốn gửi" class="input-xlarge"></textarea>
           
          </div>

            <button class="btn btn-success" type="submit">Gửi đi</button>

        </fieldset>
      </form>
		</div>
	</div>
	<div class="row">
		<div class="span12">
			<iframe style="width:100%; height:300; border: 0px" scrolling="no" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3833.9182725080154!2d108.19771841473188!3d16.069730288880603!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314218524679992f%3A0xee2e61a9b9c0223c!2zTMOqIFF1YW5nIFN1bmcsIFRoYW5oIEtow6osIMSQw6AgTuG6tW5nLCBWaWV0bmFt!5e0!3m2!1sen!2s!4v1503418157674" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</div>
</div>
@stop