<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class PermissionMW
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $check = Auth::user()->role;
        if (strpos($role,$check) === false) {
            $request->session()->flash('msg','Bạn không có quyền truy cập vào mục này');
            return redirect()->route('admin.index.index');
        }
        return $next($request);
    }
}
