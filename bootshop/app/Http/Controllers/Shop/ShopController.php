<?php

namespace App\Http\Controllers\Shop;
use Mail;
use Cart;
use App\Http\Requests\VCustomer;
use App\Http\Requests\RegisterRequest;
use App\Model\Product;
use App\Model\Category;
use App\Model\Contact;
use App\Model\Thumbnail;
use App\Model\Customer;
use App\Model\Bill;
use App\Model\User;
use App\Model\Bill_Detail;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ShopController extends Controller
{
    public function detail($slug,$id="", Request $request){
        $objDetail = Product::where('id','=',$id)->where('trash','=','1')->first();
        $cat_id = $objDetail->cat_id;
        $objRelated = Product::where('cat_id','=',$cat_id)->where('id','<>',$id)->limit(3)->get();
        $ThumbImages = Thumbnail::where('product_id','=',$id)->limit(3)->get();
        // Count view
        $count_number = $objDetail->views;
        $count = $count_number + 1;
        DB::table('products')
            ->where('id','=',$id)
            ->update(['views'=>$count]);

        $title_product = $objDetail->name;
        return view('shop.shop.detail',compact('objDetail','objRelated','ThumbImages','title_product'));
        
    }
    public function shop(){
        $objShop = Product::where('trash','=','1')->orderBy('id','DESC')->paginate(12);
        return view('shop.shop.shop',compact('objShop'));
    }
    public function offers(){
        $objShop = Product::where('trash','=','1')->where('promotion_price','<>',0)->orderBy('id','DESC')->paginate(6);
        return view('shop.shop.offer',compact('objShop'));
    }

    // public function postSearch(Request $request){
    //     $cat_id = $request->cat_id;
    //     $value = $request->value;
    //     $value1 = $request->value1;
    //     $value2 = $request->value2;
    //     if ($cat_id == 0) {
    //         $Result = DB::table('products')
    //         ->where('name','LIKE',"%$value%")
    //         ->where('unit_price','>=',$value1)
    //         ->where('unit_price','=<',$value2)
    //         ->orderBy('id','DESC')
    //         ->paginate(6);
    //     }else{
    //         $Result = DB::table('products')
    //         ->join('categories','categories.id','=','products.cat_id')
    //         ->where('products.name','LIKE',"%$value%")
    //         ->where('products.unit_price','>=',$value1)
    //         ->where('products.unit_price','=<',$value2)
    //         ->where('categories.id','=',$cat_id)
    //         ->orderBy('products.id','DESC')
    //         ->paginate(6);
    //     }
    //     if (sizeof($Result) ==0) {
    //         $request->session()->flash('msgSearch',"Không có kết quả phù hợp với keyword : {$value}");
    //         //return redirect()->route('aboutme.news.search');
    //     }
        //return view('shop.shop.search',compact('Result'));
   // }
    public function postSearch(Request $request){
        $cat_id = $request->cat_id;
        $value = $request->value;
        $value1 = $request->value1;
        $value2 = $request->value2;
        if ($cat_id == 0) {
            $Result = DB::table('products')
            ->orderBy('id','DESC');
            if(isset($value)) {
                $Result -> where('name','LIKE',"%$value%");
            }
            if (isset($value1)) {
                $Result -> where('unit_price','>=',$value1);
            }
            if (isset($value2)) {
                $Result -> where('unit_price','<=',$value2);
            }
            $search = $Result->paginate(6);
        }else{
            $Result = DB::table('products')
            ->join('categories','categories.id','=','products.cat_id')
            ->where('categories.id','=',$cat_id)
            ->orderBy('products.id','DESC');
            if(isset($value)) {
                $Result -> where('name','LIKE',"%$value%");
            }
            if (isset($value1)) {
                $Result -> where('unit_price','>=',$value1);
            }
            if (isset($value2)) {
                $Result -> where('unit_price','<=',$value2);
            }
            $search = $Result->paginate(6);
        }
        if (sizeof($search) ==0) {
            $request->session()->flash('msgSearch',"Không có kết quả phù hợp");
            //return redirect()->route('aboutme.news.search');
        }
        return view('shop.shop.search',compact('search'));
    }

    public function addItem($id,Request $request){
        $Products = DB::table('products')->where('id','=',$id)->first();
        $unit_price = $Products->unit_price;
        $promotion_price = $Products->promotion_price;
        if ($promotion_price == 0) {
            $price = $unit_price;
        }else{
            $price = $promotion_price;
        }
        Cart::add([
                'id' => $id , 
                'name' => $Products->name ,
                'qty' => 1,
                'price' => $price,
                'images' => $Products->images,
            ]
        );
        return view('shop.shop.addCart',compact('updateCart'));
    }
    public function delItem($id,Request $request){
       Cart::remove($id);
       $request->session()->flash('msgrm','Đã xóa sản phẩm khỏi giỏ hàng!');
       return redirect()->route('shop.shop.cart');
    }

    public function updateCart(Request $request){
        $id = $request->id;
        $qty = $request->qty;
        Cart::update($id,$qty);
        $updateCart = Cart::content();
       return view('shop.shop.update',compact('updateCart'));
    }

    public function cart(){
        $arProducts = Cart::content();
        $total = Cart::subtotal();
      //  print($arProducts);
    //    var_dump($arProducts);
        return view('shop.shop.cart',compact('arProducts','total'));
    }

    public function category($slug,$id){
        $ShopCat = DB::table('products')
            ->join('categories','categories.id','=','products.cat_id')
            ->where('products.cat_id','=',$id)
            ->where('trash','=','1')
            ->orderBy('products.id','DESC')
            ->select('products.id as nid','products.name as title','products.description','products.images','products.unit_price','products.promotion_price','categories.name as cname')
            ->paginate(6);
        $cat_name = Category::select('name')->where('id','=',$id)->first();
    	return view('shop.shop.category',compact('ShopCat','cat_name'));
    }
    public function contact(){
    	return view('shop.shop.contact');
    }
    public function postContact(Request $request){
        $name = trim(stripslashes($request->name));
        $email = trim(stripslashes($request->email));
        $subject = trim($request->subject);
        $content = trim(stripslashes($request->message));
        $arItem = array(
            'name' => $name,
            'email' => $email,
            'subject' => $subject,
            'message' => $content,
            'status' => 0
        );
        /*Mail::send('shop.index.mail', array('name'=>$name,'email'=>$email,'subject' => $subject, 'content'=>$content), function($message){
            $message->to('batesgillvn@gmail.com', 'Guests')->subject('Thông báo đến từ Khách hàng!');
        });*/
        if(Contact::insert($arItem)){
            $request->session()->flash('msg','Cảm ơn đã liên hệ, chúng tôi sẽ phản hồi lại sớm nhất ^^ !');
            return redirect()->route('shop.shop.contact');
        }else{
            $request->session()->flash('msg','Có gì đó không đúng, vui lòng thử lại!');
            return redirect()->route('shop.shop.contact');
        }
    }
    public function register(){
    	return view('shop.shop.register');
    }
    public function postRegister(RegisterRequest $request){
        $username = $request->username;
        $password = bcrypt($request->password);
        $email = $request->email;
        $phone = $request->phone;
        $fullname = $request->fullname;
        $address = $request->address;
        $note = $request->note;
        if ($note == "") {
            $note = "";
        }

        $arItem = array(
            'username' => $username,
            'password' => $password,
            'email' => $email,
            'phone' => $phone,
            'fullname' => $fullname,
            'address' => $address,
            'note' => $note,
            'role' => 3
        );
        if(User::insert($arItem)){
            $request->session()->flash('msg','Chúc mừng, bạn đã đăng ký thành công!');
            return redirect()->route('shop.shop.register');
        }else{
            $request->session()->flash('msg','Đã xảy ra lỗi!');
            return redirect()->route('shop.shop.register');
        }
    }
    public function step1(){
        return view('shop.shop.step1');
    }
    public function postStep1(Request $request){
        $username = $request->username;
        $password = $request->password;
        if ( Auth::attempt([
            'username' => $username, 
            'password' => $password
        ]) ) {
            //Ok
            /*$arUser = Auth::user();*/
            return redirect()->route('shop.shop.step2');
        }else{
            $request->session()->flash('msg','Username hoặc password không đúng');
            return back();
        }
    }
    public function step2(Request $request){
        $checkCart = Cart::content();
        return view('shop.shop.step2',compact('checkCart'));
    }
    public function postStep2(VCustomer $request){
        $fullname = $request->fullname;
        $phone = $request->phone;
        $email = $request->email;
        $address = $request->address;
        $note = $request->note;
        if ($note == "" || $email =="") {
            $note = "";
            $email = "";
        }
        $arCustomer = array(
            'name' => $fullname,
            'email' => $email,
            'phone' => $phone,
            'address' => $address,
            'note' => $note,
        );
        $request->session()->put('arCustomer', $arCustomer);
        return redirect()->route('shop.shop.payment');
    }
    public function payment(){
        $checkCart = Cart::content();
        return view('shop.shop.payment',compact('checkCart'));
    }
    public function postPayment(Request $request){
        $idPayment = $request->payment;
        
        if ($idPayment == 1) {
            $request->session()->put('payment',$idPayment);
            return redirect()->route('shop.shop.confirm');
        }else{
            if ($request->check =='ok') {
                $request->session()->put('payment',$idPayment);
                return redirect()->route('shop.shop.confirm');
            }else{
                $request->session()->flash('msg','Bạn chưa thanh toán, vui lòng kiểm tra lại!');
                return back();
            }
        }  
    }
    public function addCoupon($coupon,Request $request){
        $checkCart = Cart::content();
        $checkCoupon = DB::table('coupons')->where('name','=',$coupon)->limit(1)->get();
        $subtotal = Cart::subtotal();   
        $giamtien = 0;
        foreach ($checkCoupon as $key => $value) {
            $name = $value->name;
            $percent = $value->percent;
        }
        if (empty($name)) {
            return "Mã giảm giá không tồn tại";

        }else{
            if ($percent == 20) {
                $giamtien = 0.8;
            }else{
                $giamtien = 0.5;
            }
            $checkCp = DB::table('coupons')->where('percent','=',$percent)->select('st')->first();
            $checkSoluong = $checkCp->st;
            if ($checkSoluong < 1) {
                return "Mã giảm giá đã hết hiệu lực";
            }else{
                $request->session()->put('coupon',$giamtien);
                $request->session()->put('pert',$percent);
                return view('shop.shop.coupon',compact('checkCart'));
            }
        }

        
    }
    public function xacnhan(Request $request){
        $checkCart = Cart::content();
        return view('shop.shop.confirm',compact('checkCart'));
    }
    public function postXacnhan(Request $request){
        if ($request->session()->has('pert')) {
            $pert = $request->session()->get('pert');
            $st = DB::table('coupons')->where('percent','=',$pert)->select('st')->first();
            $reduce = $st->st - 1;
            DB::table('coupons')
                ->where('percent','=',$pert)
                ->update(['st'=> $reduce]);
        }
        if ($request->session()->has('arCustomer')) {
            $arCustomer = $request->session()->get('arCustomer');
            Customer::insert($arCustomer);
        }
        
        $idCustomer = DB::table('customer')->max('id');
        // Bill insert
        $subtotal = Cart::subtotal(0,'','');
        
        if ($request->session()->has('coupon')) {
            $coupon = $request->session()->get('coupon');
            $tongtien = $subtotal * $coupon;
        }else{
            $tongtien = $subtotal;
        }
        
        if ($request->session()->has('payment')) {
            $payment = $request->session()->get('payment');
            if ($payment == 1) {
                $checkpay = 2;
            }else{
                $checkpay = 1;
            }
        }
        $arBill = array(
            'customer_id' => $idCustomer,
            'total' => $tongtien,
            'payment' => $payment,
            'checkpay' => $checkpay,
        );
        Bill::insert($arBill);

        $idBill = DB::table('bills')->max('id');
        // Bill detail insert
        $Cart = Cart::content();
        foreach ($Cart as $key => $arBill) {
            $id = $arBill->id;
            $qty = $arBill->qty;
            Bill_Detail::insert([
                'id_bill' => $idBill,
                'id_product' => $id,
                'quantity' =>$qty,
                'status' => 1,
            ]);
        }

        $request->session()->forget('coupon');
        $request->session()->forget('arCustomer');     
        $request->session()->forget('payment');

        Cart::destroy();

        return redirect()->route('shop.shop.checkout');
        
    }
    // Old post step2
    public function success(VCustomer $request){
        $Cart = Cart::content();
        foreach ($Cart as $key => $value) {
            $id = $value->id;
            $qty = $value->qty;
            $name_product = $value->name;
        }
        $checkPayment = $request->payment;
        if ($checkPayment == 1) {
            // Customer insert
            $fullname = $request->fullname;
            $phone = $request->phone;
            $email = $request->email;
            $address = $request->address;
            $note = $request->note;
            if ($note == "" || $email =="") {
                $note = "";
                $email = "";
            }
            $arCustomer = array(
                'name' => $fullname,
                'email' => $email,
                'phone' => $phone,
                'address' => $address,
                'note' => $note,
            );

            Customer::insert($arCustomer);

            $idCustomer = DB::table('customer')->max('id');
            // Bill insert
            $total = Cart::subtotal();
            $payment = $request->payment;
            $arBill = array(
                'customer_id' => $idCustomer,
                'total' => $total,
                'payment' => $payment,
                'checkpay' => 2,
            );
            Bill::insert($arBill);

            $idBill = DB::table('bills')->max('id');
            // Bill detail insert
            foreach ($Cart as $key => $arBill) {
                $id = $arBill->id;
                $qty = $arBill->qty;
                Bill_Detail::insert([
                    'id_bill' => $idBill,
                    'id_product' => $id,
                    'quantity' =>$qty,
                    'status' => 1,
                ]);
            }
            Cart::destroy();
            return redirect()->route('shop.shop.checkout');
        }else{
            if ($request->check =='ok') {
                // Customer insert
                $fullname = $request->fullname;
                $phone = $request->phone;
                $email = $request->email;
                $address = $request->address;
                $note = $request->note;
                if ($note == "" || $email =="") {
                    $note = "";
                    $email = "";
                }
                $arCustomer = array(
                    'name' => $fullname,
                    'email' => $email,
                    'phone' => $phone,
                    'address' => $address,
                    'note' => $note,
                );
                Customer::insert($arCustomer);
                $idCustomer = DB::table('customer')->max('id');
                // Bill insert
                $total = Cart::subtotal();
                $payment = $request->payment;
                $arBill = array(
                    'customer_id' => $idCustomer,
                    'total' => $total,
                    'payment' => $payment,
                    'checkpay' => 1,
                );
                Bill::insert($arBill);

                $idBill = DB::table('bills')->max('id');
                // Bill detail insert
                foreach ($Cart as $key => $arBill) {
                    $id = $arBill->id;
                    $qty = $arBill->qty;
                    Bill_Detail::insert([
                        'id_bill' => $idBill,
                        'id_product' => $id,
                        'quantity' =>$qty,
                        'status' => 1,
                    ]);
                }
                Cart::destroy();
                return redirect()->route('shop.shop.checkout');
            }else{
                $request->session()->flash('msg','Bạn chưa thanh toán, vui lòng kiểm tra lại!');
                return redirect()->route('shop.shop.step2');
            }
        }
    }
    // end
    public function checkout(){
        $checkCart = Cart::content();
        return view('shop.shop.checkout',compact('checkCart'));
    }
    public function demo(){
        return view('shop.shop.demo');
    }
}
