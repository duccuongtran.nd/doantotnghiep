<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Bootshop - VNE
    @if(isset($title_product))
    - {{ $title_product }}
	  @endif
	  @if(isset($cat_name))
	    - {{ $cat_name->name }}
	  @endif
	  {{ Request::segment(1) == 'specials-offer.html' ? '- Sản phẩm đang được khuyến mãi' : null }}
	  {{ Request::segment(1) === 'contact' ? '- Liên hệ' : null }}
	  {{ Request::segment(1) === 'register' ? '- Đăng ký' : null }}
	  {{ Request::segment(1) === 'cart' ? '- Giỏ hàng' : null }}
	  {{ Request::segment(2) === 'step1' ? '- Thanh toán b1' : null }}
	  {{ Request::segment(2) === 'step2' ? '- Thanh toán b2' : null }}
	  {{ Request::segment(2) === 'thanh-toan.html' ? '- Chọn hình thức thanh toán' : null }}
	  {{ Request::segment(2) === 'xac-nhan-don-hang.html' ? '- Xác nhận mua hàng' : null }}
  </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
<!--Less styles -->
   <!-- Other Less css file //different less files has different color scheam
	<link rel="stylesheet/less" type="text/css" href="themes/less/simplex.less">
	<link rel="stylesheet/less" type="text/css" href="themes/less/classified.less">
	<link rel="stylesheet/less" type="text/css" href="themes/less/amelia.less">  MOVE DOWN TO activate
	-->
	<!--<link rel="stylesheet/less" type="text/css" href="themes/less/bootshop.less">
	<script src="themes/js/less.js" type="text/javascript"></script> -->
	
<!-- Bootstrap style --> 
	
    <link id="callCss" rel="stylesheet" href="{{ $publicUrl }}/themes/bootshop/bootstrap.min.css" media="screen"/>
    <link href="{{ $publicUrl }}/themes/css/base.css" rel="stylesheet" media="screen"/>
<!-- Bootstrap style responsive -->	
	<link href="{{ $publicUrl }}/themes/css/bootstrap-responsive.min.css" rel="stylesheet"/>
	<link href="{{ $publicUrl }}/themes/css/font-awesome.css" rel="stylesheet" type="text/css">
<!-- Google-code-prettify -->	
	<link href="{{ $publicUrl }}/themes/js/google-code-prettify/prettify.css" rel="stylesheet"/>
<!-- fav and touch icons -->
    <link rel="shortcut icon" href="{{ $publicUrl }}/themes/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ $publicUrl }}/themes/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ $publicUrl }}/themes/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ $publicUrl }}/themes/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="{{ $publicUrl }}/themes/images/ico/apple-touch-icon-57-precomposed.png">
    
    <script type="text/javascript" src="{{ $publicUrl }}/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="{{ $publicUrl }}/js/ccscript.js"></script>
    <script type="text/javascript" src="{{ $publicUrl }}/js/addCart.js"></script>
    <script type="text/javascript" src="{{ $publicUrl }}/js/coupon.js"></script>
    <script type="text/javascript" src="{{ $publicUrl }}/js/flycolor.js"></script>
    <script type="text/javascript" src="{{ $publicUrl }}/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="{{ $publicUrl }}/ckfinder/ckfinder.js"></script>
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
	<style type="text/css" id="enject"></style>

  </head>
<body>
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'vi', includedLanguages: 'en,lo,vi', layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<div id="header">
<div class="container">
<div id="welcomeLine" class="row">
	<div class="span6">Xin chào<strong>
		@if(isset(Auth::user()->username))
			<a href="{{route('admin.checkorder.index')}}">{{Auth::user()->username}}</a>
		@else
			Khách!
		@endif
	</strong></div>
	<div class="span6">
	</div>
</div>
<!-- Navbar ================================================== -->
<div id="logoArea" class="navbar">
<a id="smallScreen" data-target="#topMenu" data-toggle="collapse" class="btn btn-navbar">
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
</a>
  <div class="navbar-inner">
    <a class="brand" href="/"><img width="120px" height="80px" src="{{ $publicUrl }}/images/lao.png" alt="Bootsshop"/></a>

		<form class="form-inline navbar-search" method="post" action="{{route('shop.shop.search')}}" >
		{{csrf_field()}}
		<input id="srchFld" name="value" placeholder="Tìm kiếm sản phẩm" required="required" class="srchTxt" type="text" />
		  <select class="srchTxt" name="cat_id">
			<option value="0">Tất cả</option>
			@foreach($CatSidebar as $arCatss)
			<option value="{{$arCatss->id}}">{{$arCatss->name}}</option>
			@endforeach
		</select> 
		  <button type="submit" id="submitButton" class="btn btn-primary">Go</button>
    </form>
    <ul id="topMenu" class="nav pull-right">
	 <li class=""><a href="{{ route('shop.shop.offer')}}">Khuyến mãi</a></li>
	 <li class=""><a href="{{route('shop.shop.contact')}}">Liên hệ</a></li>
	 <li class=""><a href="{{ route('shop.shop.register')}}">Đăng ký</a></li>
	 <li class="">
	 <a href="{{route('auth.auth.login')}}" style="padding-right:0"><span class="btn btn-large btn-success">Đăng nhập</span></a>
	<div id="login" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3>Login Block</h3>
		  </div>
		  <div class="modal-body">
			<form class="form-horizontal loginFrm">
			  <div class="control-group">								
				<input type="text" id="inputEmail" placeholder="Email">
			  </div>
			  <div class="control-group">
				<input type="password" id="inputPassword" placeholder="Password">
			  </div>
			
			</form>		
			<button type="submit" class="btn btn-success">Sign in</button>
			<button class="btn" data-dismiss="modal" aria-hidden="true">Đóng</button>
		  </div>
	</div>
	</li>
    </ul>
  </div>
</div>
</div>
</div>
<!-- Header End====================================================================== -->
