<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'price' => 'required|numeric',
            'promotion' => 'required|numeric',
            'images' =>  'image',
            'description' => 'required',
            'detail' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhập tên sản phẩm', 
            'price.required' => 'Giá tiền không được bỏ trống',
            'price.numeric' => 'Tiền phải là số -_-',
            'promotion.required' => 'Trường hợp không khuyến mãi, thì giá khuyến mãi bằng 0',
            'promotion.numeric' => 'Tiền phải là số -_-',
            'images.image' => 'Định dạng không được hỗ trợ', 
            'description.required' => 'Vui lòng nhập mô tả',
            'detail.required' => 'Vui lòng nhập chi tiết ', 

        ];
    }
}
