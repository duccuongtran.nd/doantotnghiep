@extends('templates.admin.master')
@section('main-content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Thông tin khách hàng</h1>
                </div>
                @if(Session::has('msg'))
                    <script> alert('{{ Session::get('msg') }}')</script>
                @endif
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('admin.checkorder.info')}}" class="fa fa-mail-reply" style="font-size:20px;">Về trang chủ</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form  action="{{ route('admin.checkorder.editInfo',$objUser->id) }}" role="form" method="POST">
                                        {{ csrf_field()}}
                                        <div class="form-group">
                                            <label>Username</label>
                                            <h5> {{$objUser->username }}</h5>
                                            {{ array_first($errors->get('username')) }}    
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Full name</label>
                                            <input class="form-control" name="fullname" value="{{$objUser->fullname }}">
                                            {{ array_first($errors->get('fullname')) }}       
                                        </div>
                                        <div class="form-group">
                                            <label>Mật khẩu </label>
                                            <input type="password" class="form-control" name="password" value="">
                                            {{ array_first($errors->get('password')) }}       
                                        </div>
                                        <div class="form-group">
                                            <label>Mật khẩu cũ</label>
                                        <input type="password" class="form-control" name="oldpassword" value="">
                                            {{ array_first($errors->get('oldpassword')) }}       
                                        </div>
                                        <div class="form-group">
                                            <label>Phone</label>
                                            <input class="form-control" name="phone" value="{{$objUser->phone }}">
                                            {{ array_first($errors->get('phone')) }}    
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" name="email" value="{{$objUser->email }}">
                                            {{ array_first($errors->get('email')) }}    
                                        </div>
                                        <div class="form-group">
                                            <label>Địa chỉ</label>
                                            <textarea class="form-control" name="address">{{$objUser->address }}</textarea>
                                            {{ array_first($errors->get('address')) }}    
                                        </div>
                                        <div class="form-group">
                                            <label>Lưu ý thêm</label>
                                            <textarea class="form-control" name="note">{{$objUser->note }}</textarea>
                                            {{ array_first($errors->get('note')) }}    
                                        </div>
                                        
                                        <div class="form-group">
                                            <input type="submit" value="Edit" class="btn btn-primary">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        <!-- /#page-wrapper -->
@stop