﻿@extends('templates.shop.master')
@section('main-content')
{{-- <div id="mainBody"> --}}
	<div class="container">
	<div class="row">
@include ('templates.shop.sidebar')
	<div class="span9">
    <ul class="breadcrumb">
		<li class="active">Đăng ký</li>
    </ul>
	<h3> Đăng ký tài khoản</h3>	
	@if(Session::has('msg'))
		<script>alert('{{Session::get('msg')}}')</script>
	@endif
	<div class="well">
	<form method="Post" action="{{route('shop.shop.register')}}" class="form-horizontal" >
		{{csrf_field()}}
		<h4>Thông tin cá nhân của bạn</h4>
			<div class="control-group">
				<label class="control-label">Username <sup>*</sup></label>
				<div class="controls">
				  <input name="username" value="{{ old('username') }}" type="text" id="inputFname1" placeholder="Username">
				  {{ array_first($errors->get('username')) }}
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Password <sup>*</sup></label>
				<div class="controls">
				  <input name="password" value="{{ old('password') }}" type="password" id="inputFname1" placeholder="Password">
				  {{ array_first($errors->get('password')) }}
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Tên đầy đủ <sup>*</sup></label>
				<div class="controls">
				  <input name="fullname" value="{{ old('fullname') }}" type="text" id="inputFname1" placeholder=" Họ tên nên có ít nhất 10 kí tự">
				  {{ array_first($errors->get('fullname')) }}
				</div>
			 </div>
			<div class="control-group">
			<label class="control-label">Số điện thoại <sup>*</sup></label>
				<div class="controls">
				  <input name="phone" value="{{ old('phone') }}" type="text" id="input_email" placeholder="Số điện thoại">
				  {{ array_first($errors->get('phone')) }}
				</div>
		   </div>
		    <div class="control-group">
				<label class="control-label">Email *<sup></sup></label>
				<div class="controls">
				  <input name="email" value="{{ old('email') }}" type="text" id="inputLnam" placeholder="Email">
				  {{ array_first($errors->get('email')) }}
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Địa chỉ *<sup></sup></label>
				<div class="controls">
				  	<textarea name="address" placeholder="Địa chỉ nên có ít nhất 10 kí tự">{{ old('address') }}</textarea>
				  	{{ array_first($errors->get('address')) }}
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Note<sup></sup></label>
				<div class="controls">
				  	<textarea name="note">{{ old('note') }}</textarea>
				</div>
			</div>	  
			<div class="control-group">
				<div class="controls">	
					<input class="btn btn-large btn-success" type="submit" value="Đăng ký" />
				</div>
			</div>		
	</form>
</div>

</div>
</div>
</div>
@stop