@extends('templates.shop.master')
@section('main-content')
{{-- <div id="mainBody"> --}}
	<div class="container">
	<div class="row">
@include ('templates.shop.sidebar')
<div class="span9">
<br class="clr"/>
<div class="tab-content">
	@if(Session::has('msgSearch'))
		{{Session::get('msgSearch')}}
	@endif
	<div class="tab-pane  active" id="blockView">
		<ul class="thumbnails">
			@foreach($Result as $arSearch)
				@php
	    			$id = $arSearch->id;
	    			$name = $arSearch->name;
	    			$description = $arSearch->description;
	    			$cutDes = str_limit($description,120,'...');
	    			$images = $arSearch->images;
	    			$unit_price = $arSearch->unit_price;
	    			$promotion_price = $arSearch->promotion_price;
	    			$slug = str_slug($name);
        			$url = route('shop.shop.detail',['slug'=>$slug,'id'=>$id]);
	    		@endphp
			<li class="span3">
			  <div class="thumbnail">
				<a href="{{ $url }}"><img src="{{ $ImagesPath }}/{{$images}}" alt="{{ $name }}" style="width:250px;height: 200px" /></a>
				<div class="caption">
				  <h6 style="max-height: 15px;padding-bottom: 20px"><a href="{{ $url }}">{{ $name }}</a></h6>
				  <p> 
					{{ $cutDes }}
				  </p>
				    <h4 style="text-align:center"><a href="javascript:;" id="{{$id}}" class="btn btn-warning addCart">Add to <i class="icon-shopping-cart"></i></a>
					  	@if($promotion_price == 0)
					  		<a class="btn btn-primary" href="#">${{$unit_price}}</a>
					  	@else
					  		<a class="btn btn-danger" style="text-decoration: line-through;" href="#">{{$unit_price}}.000 VNĐ</a>
					  		<a class="btn btn-primary" href="#">{{$promotion_price}}.000 VNĐ</a>
					  	@endif
					  </h4>
				</div>
			  </div>
			</li>
			@endforeach
		  </ul>
	<hr class="soft"/>
	</div>
</div>

	<div class="pagination">
		{{ $Result->links()}}
	</div>
		<br class="clr"/>
</div>
</div>
</div>
@stop