@extends('templates.admin.master')
@section('main-content')
        <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    <a href="{{ route('admin.contact.index') }}"> <<- Quay lại</a>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <CENTER>Contact Detail</CENTER>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Full name:</label>
                                            <p>{{ $objItem->name }}</p>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label>Email:</label>
                                            <p>{{ $objItem->email }}</p>
                                        </div>  
                                        <hr>                                     
                                        <div class="form-group">
                                            <label>Phone:</label>
                                            <p>{{ $objItem->phone }}</p>
                                        </div>   
                                        <hr>
                                        <div class="form-group">
                                            <label>Date:</label>
                                            <p>{{ $objItem->created_at }}</p>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label>Subject:</label>
                                            <textarea disabled rows="5"  class="form-control">{{ $objItem->subject }}</textarea>
                                        </div>  
                                        <div class="form-group">
                                            <label>Content:</label>
                                            <textarea disabled rows="5"  class="form-control">{{ $objItem->message }}</textarea>
                                        </div>                                     
                                </div>                             
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div>
        <!-- end page-wrapper -->
 @stop