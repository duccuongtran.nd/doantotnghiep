<?php

namespace App\Http\Controllers\Admin;
use App\Model\Product;
use App\Model\Category;
use App\Model\Thumbnail;
use App\Http\Requests\ProductRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index(){
    	$objProducts =
             DB::table('products')
            ->join('categories','products.cat_id','=','categories.id')
            ->where('products.trash','=',1)
            ->orderBy('products.id','DESC')
            ->select('products.id as pid','products.name as p_name','products.images','categories.name as cat_name','products.unit_price')
            ->get();
    	return view('admin.product.index',compact('objProducts'));
    }
    public function trash(){
        $objTrash =
             DB::table('products')
            ->join('categories','products.cat_id','=','categories.id')
            ->where('products.trash','=',0)
            ->orderBy('products.id','DESC')
            ->select('products.id as pid','products.name as p_name','products.images','categories.name as cat_name','products.unit_price')
            ->get();
        return view('admin.product.trash',compact('objTrash'));
    }
    public function getAdd(){
        $objCats = Category::all();
        return view('admin.product.add',compact('objCats'));
    }
    public function postAdd(ProductRequest $request){
        //dd($request->all());
        
        $name = $request->name;
        $cid = $request->category;
        $views = 0;
        $description = $request->description;
        $detail = $request->detail;
        $picture =$request->images;
        $unit_price =$request->price;
        $promotion_price =$request->promotion;
        $trash=1;
        $arItem = array(
            'name' => $name,
            'cat_id' => $cid,
            'description' => $description,
            'detail' => $detail,
            'views' => $views,
            'unit_price' => $unit_price,
            'promotion_price' => $promotion_price,
            'trash' => $trash,
        );
	      if ($picture != '') {
	        $tmp = $request->file('images')->store('/public/files');
	        $pic = explode('/', $tmp);
	        $picture = end($pic);
	        $arItem['images'] = $picture;
	      }else{
	        $arItem['images'] = "default.png";
	      }

        if(Product::insert($arItem)){
                if ($request->thumbnail != "") {  
                
                    $getId = DB::table('products')->max('id');
                    foreach ($request->thumbnail as $thumbnail) {
                        $filename = $thumbnail->store('/public/thumbnail');
                        Thumbnail::insert([
                            'name' => $filename,
                            'product_id' => $getId
                        ]);
                    }
            }
            $request->session()->flash('msg','Thêm thành công!');
            return redirect()->route('admin.product.index');
        }else{
            $request->session()->flash('msg','Đã xảy ra lỗi!');
            return redirect()->route('admin.product.index');
        }   
    }
    public function getEdit($id, Request $request){
        $objItem = Product::FindOrFail($id);
        $objCats = Category::all();
        return view('admin.product.edit',compact('objItem','objCats'));
    }
    public function postEdit($id, ProductRequest $request){
        $objItem= Product::find($id);
        $name = $request->name;
        $cid = $request->category;
        $views = 0;
        $description = $request->description;
        $detail = $request->detail;
        $picture =$request->images;
        $unit_price =$request->price;
        $promotion_price =$request->promotion;
        $trash=1;
        $oldPic = $objItem->images;
        if ($promotion_price > $unit_price) {
           $objItem->promotion_price = 0; 
        }else{
            $objItem->promotion_price = $promotion_price;
        }
        $objItem->name = $name;
        $objItem->cat_id = $cid;
        $objItem->views = $views;
        $objItem->description = $description;
        $objItem->name = $name;
        $objItem->detail = $detail;
        $objItem->unit_price = $unit_price;
        $objItem->trash = $trash;
        if ($picture != '') {
            $tmp = $request->file('images')->store('/public/files');
            $pic = explode('/', $tmp);
            $picture = end($pic);
            // xóa ảnh cũ       
            if ($oldPic != '' && $oldPic != "default.png") {
                Storage::delete('/public/files/'.$oldPic);
            }
            $objItem->images = $picture;
        }
            if ($request->thumbnail != "") {   
                $arThumb = Thumbnail::where('product_id','=',$id)->get(); 
                    foreach ($arThumb as $Thumbnail) {
                        $picture = $Thumbnail->name;
                        if ($picture != '') {
                            Storage::delete($picture);
                        }
                    }
                DB::table('thumbnail')->where('product_id', '=', $id)->delete();
                foreach ($request->thumbnail as $thumbnail) {
                    $filename = $thumbnail->store('/public/thumbnail');
                    Thumbnail::insert([
                        'name' => $filename,
                        'product_id' => $id
                    ]);
                }
            }
        if($objItem->update()){
            $request->session()->flash('msg','Sửa thành công');
            return redirect()->route('admin.product.index');
        }else{
            $request->session()->flash('msg','Đã xảy ra lỗi!');
            return redirect()->route('admin.product.index');
        }
    }
    public function destroy(Request $request){
        $id = $request->delete;
        
        $objItem = Product::findOrFail($id);
        foreach ($objItem as $key => $value) {
            $oldPic = $value->images;
            if ($oldPic != "" && $oldPic != "default.png") {
            Storage::delete('/public/files/'.$oldPic);
            }
        }
        
        $arThumb = Thumbnail::where('product_id','=',$id)->get(); 
        foreach ($arThumb as $Thumbnail) {
            $picture = $Thumbnail->name;
            if ($picture != '') {
                Storage::delete($picture);
            }
        } 
        DB::table('thumbnail')->where('product_id', '=', $id)->delete();
        if (is_array($id)){
            Product::destroy($id);

            $request->session()->flash('msg','Xóa thành công!');
            return redirect()->route('admin.product.index');
        }else{
            Product::findOrFail($id)->delete();
            
            $request->session()->flash('msg','Xóa thành công!');
            return redirect()->route('admin.product.index');
        }
    }
    public function temp($id, Request $request){
        DB::table('products')
        ->where('id','=', $id)
        ->update(['trash' => 0]);
        $request->session()->flash('msg','Một sản phẩm đã được di chuyển tới thùng rác!');
        return redirect()->route('admin.product.index');
    }
    public function restore($id, Request $request){
        DB::table('products')
        ->where('id','=', $id)
        ->update(['trash' => 1]);
        $request->session()->flash('msg','1 sản phẩm đã được khôi phục.');
        return redirect()->route('admin.product.index');
    }
}
