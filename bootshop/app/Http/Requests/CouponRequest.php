<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:coupons,name|min:6|max:6',
            'st' => 'required|numeric',
            'percent' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Trường này không được bỏ trống', 
            'name.unique' => 'Mã giảm giá đã tồn tại',
            'name.min' => 'Mã giảm phải là 6 kí tự', 
            'name.max' => 'Mã giảm phải là 6 kí tự', 
            'st.required' => 'Trường này không được bỏ trống',
            'st.numeric' => 'Số lượng phải là số', 
            'percent.required' => 'Trường này không được bỏ trống', 
        ];
    }
}
