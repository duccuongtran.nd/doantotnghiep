<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Bill_Detail extends Model
{
    protected $table = 'bill_detail';
    protected $primaryKey ='id';
    public $timestamps = true;
}
