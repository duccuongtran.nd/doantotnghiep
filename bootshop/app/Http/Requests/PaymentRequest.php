<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment' => 'required|unique:payments,name',
        ];
    }
    public function messages()
    {
        return [
            'payment.required' => 'This field could not be empty', 
            'payment.unique' => 'This name already exists',      
        ];
    }
}
