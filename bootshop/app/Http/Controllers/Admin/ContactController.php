<?php

namespace App\Http\Controllers\Admin;
use App\Model\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    public function index(){
    	$objContact = Contact::paginate(10);
    	return view('admin.contact.index',compact('objContact'));
    }
    public function getView($id){
    	$objItem = Contact::findOrFail($id);
      $status = $objItem->status;
      if ($status != 1) {
          DB::table('contacts')
         ->where('id','=', $id)
         ->update(['status' => 1]);
      }
      return view('admin.contact.view',compact('objItem'));
    }
    public function del($id, Request $request){
        $objItem = Contact::FindOrFail($id);
     
        if($objItem->delete()){
          $request->session()->flash('msg','Xóa thành công');
          return redirect()->route('admin.contact.index');
          }else{
          $request->session()->flash('msg','Đã có lỗi xảy ra!');
          return redirect()->route('admin.contact.index');
        }
    }
}
