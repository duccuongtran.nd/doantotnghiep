@extends('templates.admin.master')
@section('main-content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Chỉnh sửa sản phẩm</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            @if(Session::has('msg'))
                <script> alert('{{ Session::get('msg') }}')</script>
            @endif
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('admin.product.index')}}" class="fa fa-mail-reply" style="font-size:20px;"> Quay lại</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    @php
                                        $urlPic = Storage::url('files/' . $objItem->images);
                                    @endphp
                                    <form  action="{{ route('admin.product.edit',$objItem->id)}}" role="form" method="POST" enctype='multipart/form-data'>
                                        {{ csrf_field()}}
                                        <div class="form-group">
                                            <label>Tên sản phẩm</label>
                                            <input name="name" value="{{$objItem->name}}" class="form-control">
                                        </div>
                                        @if ($errors->has('name'))
                                        <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            {{ array_first($errors->get('name')) }}
                                        </div>
                                        @endif  
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Giá gốc</label>
                                                <div class="input-group">
                                                    <input type="text" name="price" class="form-control" value="{{$objItem->unit_price}}" placeholder="Product Price" value="">
                                                    <span class="input-group-addon">.000 VNĐ</span>
                                                </div>
                                            </div>
                                        </div>
                                        @if ($errors->has('price'))
                                        <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            {{ array_first($errors->get('price')) }}
                                        </div>
                                        @endif  
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Giá khuyến mãi</label>
                                                <div class="input-group">
                                                    <input type="text" name="promotion" class="form-control" placeholder="Promotion Price"value="{{$objItem->promotion_price}}"">
                                                    <span class="input-group-addon">.000 VNĐ</span>
                                                </div>
                                            </div>
                                        </div>
                                        @if ($errors->has('promotion'))
                                        <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            {{ array_first($errors->get('promotion')) }}
                                        </div>
                                        @endif  
                                        <div class="form-group">
                                            <label>Danh mục</label>
                                            <select name="category" class="form-control">
                                                @foreach($objCats as $arCats)
                                                    @php
                                                    if ($arCats->id == $objItem->cat_id) {
                                                        $selected = "selected='selected'";
                                                    }else{
                                                        $selected ="";
                                                    }
                                                    @endphp
                                                    <option {{ $selected }} value="{{$arCats->id}}">{{$arCats->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Hình ảnh</label>
                                            <input type="file" name="images">
                                            <img src="{{$urlPic}}" width="150px">
                                        </div>
                                        @if ($errors->has('images'))
                                        <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            {{ array_first($errors->get('images')) }}
                                        </div>
                                        @endif  
                                        <div class="form-group" style="float: right;">
                                            <label>Ảnh thu nhỏ</label>
                                            @php
                                                $objThumb = DB::table('thumbnail')->where('product_id', '=', $objItem->id)->get();
                                                $urlThumb = Storage::url('app/');
                                            @endphp
                                            <input type="file" name="thumbnail[]" multiple="true">
                                                @foreach($objThumb as $arThumb)
                                                    <img src="{{$urlThumb}}/{{$arThumb->name}}" width="70px">
                                                @endforeach
                                        </div>
                                        @if ($errors->has('thumbnail'))
                                        <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            {{ array_first($errors->get('thumbnail')) }}
                                        </div>
                                        @endif  
                                        <div class="form-group">
                                            <label>Mô tả</label>
                                            <textarea name="description" class="form-control" rows="3">{{$objItem->description}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Chi tiết</label>
                                            <textarea name="detail" id="detail" class="ckeditor" rows="3">{{$objItem->detail}}</textarea>
                                        </div>
                                        <script type="text/javascript">
                                        CKEDITOR.replace( 'detail',
                                                {
                                                filebrowserBrowseUrl : 'http://bootshop.com/resources/assets/templates/shop/ckfinder/ckfinder.html',
                                                filebrowserImageBrowseUrl : 'http://bootshop.com/resources/assets/templates/shop/ckfinder/ckfinder.html?type=Images',
                                                filebrowserFlashBrowseUrl : 'http://bootshop.com/resources/assets/templates/shop/ckfinder/ckfinder.html?type=Flash',
                                                filebrowserUploadUrl : 'http://bootshop.com/resources/assets/templates/shop/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                                                filebrowserImageUploadUrl : 'http://bootshop.com/resources/assets/templates/shop/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                                                filebrowserFlashUploadUrl : 'http://bootshop.com/resources/assets/templates/shop/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                                            });
                                        </script>
                                        @if ($errors->has('detail'))
                                        <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            {{ array_first($errors->get('detail')) }}
                                        </div>
                                        @endif                              
                                        <button type="submit" class="btn btn-primary">Chỉnh sửa</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        <!-- /#page-wrapper -->
@stop