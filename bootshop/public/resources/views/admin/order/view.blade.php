@extends('templates.admin.master')
@section('main-content')
        <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    <a href="{{ route('admin.order.index') }}"> <<- Back to Orders</a>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <CENTER>Order Detail</CENTER>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form method="post" action="{{route('admin.order.view',$objOrder->bid)}}">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label>Full name:</label>
                                            <p>{{ $objOrder->name }}</p>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label>Email:</label>
                                            <p>{{ $objOrder->email }}</p>
                                        </div>  
                                        <hr>                                     
                                        <div class="form-group">
                                            <label>Phone:</label>
                                            <p>{{ $objOrder->phone }}</p>
                                        </div>   
                                        <hr>
                                        <div class="form-group">
                                            <label>Date Order:</label>
                                            <p>{{ $objOrder->date }}</p>
                                        </div>
                                        <div class="form-group">
                                            <label>Address:</label>
                                            <textarea class="form-control" disabled="disabled">{{ $objOrder->address }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Check Payment:</label>
                                            @php
                                                if ($objOrder->payment == 1) {
                                                    $pay = "Giao hàng tận nhà ";
                                                }else{
                                                    $pay = "Thông qua Paypal";
                                                }

                                            @endphp
                                            <p style="color:red">{{ $pay }}</p>
                                            <select class="form-control" name="checkpay">
                                                @php
                                                for ($i=1; $i <= 2 ; $i++) { 
                                                        if ($i == 1) {
                                                            $checkpay = "Đã thanh toán";
                                                       }else{
                                                            $checkpay = "Chưa thanh toán";
                                                       }
                                                        if ($i == $objOrder->checkpay) {
                                                            $selected = "selected='selected'";
                                                        }else{
                                                            $selected = "";
                                                        }
                                                        echo "<option $selected value='$i'>$checkpay</option>";
                                                    }
                                                @endphp
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Status:</label>
                                            <select class="form-control" name="status">
                                                @php
                                                    for ($i=1; $i <= 4 ; $i++) { 
                                                        $status = $objOrder->status;
                                                        if ($i == 1) {
                                                            $tt = 'Đang chờ';
                                                        }if ($i == 2) {
                                                            $tt = 'Đã được gửi đi';
                                                        }if ($i == 3) {
                                                            $tt = 'Hoàn tất';
                                                        }if ($i == 4) {
                                                            $tt = 'Từ chối';
                                                        }
                                                        if ($i == $status) {
                                                            $selected = "selected='selected'";
                                                        }else{
                                                            $selected = "";
                                                        }
                                                        echo "<option $selected value='$i'>$tt</option>";
                                                    }
                                                @endphp
                                            </select>
                                        </div>
                                        <div class="form-group">    
                                                <table width="100%" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <th>Product name</th>
                                                    <th>Quantity </th>
                                                    <th>Unit Price</th>
                                                    <th>Promotion Price</th>
                                                </thead>
                                                <tbody>
                                            @foreach($objProducts as $arProducts)
                                                    <tr class="odd gradeX">
                                                        <td>
                                                            {{$arProducts->name }}
                                                        </td>
                                                        <td>
                                                            {{$arProducts->quantity }}
                                                        </td>
                                                        <td>
                                                            {{$arProducts->unit_price }}.000 VNĐ
                                                        </td>
                                                        <td>
                                                            {{$arProducts->promotion_price }}.000 VNĐ
                                                        </td>
                                                    </tr>
                                            @endforeach
                                                </tbody>
                                            </table>
                                            <h4 style="float:right;color:green">Total : {{ $objOrder->total }}0 VNĐ</h4><br>
                                        </div>
                                        <center><button class="btn btn-success">Save</button></center>
                                    </form>                                   
                                </div>                             
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div>
        <!-- end page-wrapper -->
 @stop