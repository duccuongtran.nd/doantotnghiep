<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Thumbnail extends Model
{
    protected $table = 'thumbnail';
    protected $primaryKey ='id';
    public $timestamps = true;
}
