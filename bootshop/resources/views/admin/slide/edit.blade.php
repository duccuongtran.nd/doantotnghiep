@extends('templates.admin.master')
@section('main-content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Slide</h1>
                </div>
                @if(Session::has('msg'))
                    <script> alert('{{ Session::get('msg') }}')</script>
                @endif
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('admin.slide.index')}}" class="fa fa-mail-reply" style="font-size:20px;"> Back to Slide Page</a>
                        </div>
                        @php
                            $urlPic = Storage::url('app/files/' . $objSlide->picture);
                        @endphp
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form  action="{{ route('admin.slide.edit',$objSlide->id)}}" role="form" method="POST" enctype='multipart/form-data' >
                                        {{ csrf_field()}}
                                        <div class="form-group">
                                            <label>Picture</label>
                                            <input  type="file" class="form-control" name="picture"> 
                                            <label>Old Picture</label><br>
                                            <img src="{{$urlPic}}" width="200px">    
                                        </div>
                                        <div class="form-group">
                                            <label>Link</label>
                                            <input class="form-control" value="{{$objSlide->link}}" name="link" required="required">    
                                        </div>

                                        <div class="form-group">
                                            <input type="submit" value="Edit Slide" class="btn btn-primary">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        <!-- /#page-wrapper -->
@stop