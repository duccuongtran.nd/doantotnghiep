﻿{{-- {{dd(Session::all())}} --}}
@include ('templates.shop.header')
@include ('templates.shop.slide')
<div id="mainBody">
	<div class="container">
	<div class="row">
@include ('templates.shop.sidebar')
		<div class="span9">
		    <div class="well well-small">
		        <h4>Sản phẩm nổi bật</h4>
		        <div class="row-fluid">
		            <div id="featured" class="carousel slide">
		                <div class="carousel-inner">
		                    <div class="item active">
		                        <ul class="thumbnails">
		                        	@foreach($FeatureProducts as $arProducts)  
			                        	@php
			                        		$slug = str_slug($arProducts->name);
                                			$url = route('shop.shop.detail',['slug'=>$slug,'id'=>$arProducts->id]);
			                        	@endphp         		
		                            <li class="span3">
		                                <div class="thumbnail">
		                                    <i class="tag"></i>
		                                    <a href="{{ $url }}"><img src="{{ $ImagesPath }}/{{$arProducts->images}}" style="height: 200px" alt="{{$arProducts->name}}"></a>
		                                    <div class="caption">
		                                        <h6 style="max-height: 15px;padding-bottom: 35px"><a href="{{ $url }}">{{$arProducts->name}}</a></h6>
		                                        <h4><a class="btn btn-success" href="{{ $url }}">Xem</a>
		                                        @if($arProducts->promotion_price == 0)
											  		<a class="btn btn-primary" href="#">{{$arProducts->unit_price}}.000 VNĐ</a>
											  	@else
											  		<a class="label label-important" style="text-decoration: line-through;" href="#">{{$arProducts->unit_price}}.000 VNĐ</a>
											  		<a class="label label-success" href="#">{{$arProducts->promotion_price}}.000 VNĐ</a>
											  	@endif</h4>
		                                    </div>
		                                </div>
		                            </li>
		                          	@endforeach
		                        </ul>
		                    </div>
		                    <div class="item">
		                        @foreach($FeatureProducts2 as $arProducts)
		                        	@php
		                        		$slug = str_slug($arProducts->name);
	                        			$url = route('shop.shop.detail',['slug'=>$slug,'id'=>$arProducts->id]);
		                        	@endphp
		                            <li class="span3">
		                                <div class="thumbnail">
		                                    <i class="tag"></i>
		                                    <a href="{{ $url }}"><img src="{{ $ImagesPath }}/{{$arProducts->images}}" style="height: 200px" alt="{{$arProducts->name}}"></a>
		                                    <div class="caption">
		                                        <h6 style="max-height: 15px;padding-bottom: 35px"><a href="{{ $url }}">{{$arProducts->name}}</a></h6>
		                                        <h4><a class="btn btn-success" href="{{ $url }}">Xem</a>
		                                        @if($arProducts->promotion_price == 0)
											  		<a class="btn btn-primary" href="#">{{$arProducts->unit_price}}.000 VNĐ</a>
											  	@else
											  		<a class="label label-important" style="text-decoration: line-through;" href="#">{{$arProducts->unit_price}}.000 VNĐ</a>
											  		<a class="label label-success" href="#">{{$arProducts->promotion_price}}.000 VNĐ</a>
											  	@endif</h4>
		                                    </div>
		                                </div>
		                            </li>
		                        @endforeach
		                    </div>
		                    
		                </div>
		                <a class="left carousel-control" href="#featured" data-slide="prev">‹</a>
		                <a class="right carousel-control" href="#featured" data-slide="next">›</a>
		            </div>
		        </div>
		    </div>
		<h4>Sản phẩm khác </h4>
			  <ul class="thumbnails">
			  @foreach($LatestProducts as $arLatest)
			  	@php
            		$slug = str_slug($arLatest->name);
        			$url = route('shop.shop.detail',['slug'=>$slug,'id'=>$arLatest->id]);
            	@endphp

				<li class="span3">
				  <div class="thumbnail">
					<a  href="{{$url}}"><img src="{{ $ImagesPath }}/{{$arLatest->images}}" style="width: 320px;height: 230px" alt="{{$arLatest->name}}"></a>
					<div class="caption">
					  <h6 style="max-height: 15px;padding-bottom: 20px"><a href="{{$url}}">{{$arLatest->name}}</a></h6>
					  <p> 
						{{substr($arLatest->description,0,50)}}
					  </p>
					 
					  <h4 style="text-align:center">
					  <a href="javascript:;" id="{{$arLatest->id}}" class="btn btn-warning addCart add-to-cart">Thêm vào <i class="icon-shopping-cart"></i></a>
					  	@if($arLatest->promotion_price == 0)
					  		<a class="btn btn-primary" href="#">{{$arLatest->unit_price}}.000 VNĐ</a>
					  	@else
					  		<a class="btn btn-danger" style="text-decoration: line-through;" href="#">{{$arLatest->unit_price}}.000 VNĐ</a>
					  		<a class="btn btn-primary" href="#">{{$arLatest->promotion_price}}.000 VNĐ</a>
					  	@endif
					  </h4>
					</div>
				  </div>
				</li>
				@endforeach
			  </ul>	
			 <center><a href="{{ route('shop.shop.shop')}}" class="btn btn-primary">Xem tất cả sản phẩm</a></center>
		</div>
		
		</div>
	</div>
@include ('templates.shop.footer')