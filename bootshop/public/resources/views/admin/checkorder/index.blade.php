@extends('templates.admin.master')
@section('main-content')
        <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    <a href="/"> <<- Trở lại trang chủ</a>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <CENTER>Đơn hàng mới nhất</CENTER>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Tên đầy đủ:</label>
                                            <p>{{ $objOrder->name }}</p>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label>Email:</label>
                                            <p>{{ $objOrder->email }}</p>
                                        </div>  
                                        <hr>                                     
                                        <div class="form-group">
                                            <label>Số điện thoại :</label>
                                            <p>{{ $objOrder->phone }}</p>
                                        </div>   
                                        <hr>
                                        <div class="form-group">
                                            <label>Ngày đặt hàng :</label>
                                            <p>{{ $objOrder->date }}</p>
                                        </div>
                                        <div class="form-group">
                                            <label>Địa chỉ :</label>
                                            <textarea class="form-control" disabled="disabled">{{ $objOrder->address }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Hình thức thanh toán:</label>
                                            @php
                                                if ($objOrder->payment == 1) {
                                                    $pay = "Thanh toán sau khi nhận hàng ";
                                                }else{
                                                    $pay = "Thanh toán thông qua Paypal";
                                                }

                                            @endphp
                                            <p style="color:red">{{ $pay }}</p>
                                            <select class="form-control" name="checkpay">
                                                @php
                                                for ($i=1; $i <= 2 ; $i++) { 
                                                        if ($i == 1) {
                                                            $checkpay = "Đã thanh toán";
                                                       }else{
                                                            $checkpay = "Chưa thanh toán";
                                                       }
                                                        if ($i == $objOrder->checkpay) {
                                                            $selected = "selected='selected'";
                                                        }else{
                                                            $selected = "";
                                                        }
                                                        echo "<option $selected disabled value='$i'>$checkpay</option>";
                                                    }
                                                @endphp
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Status:</label>
                                            <select class="form-control" name="status">
                                                @php
                                                    for ($i=1; $i <= 4 ; $i++) { 
                                                        $status = $objOrder->status;
                                                        if ($i == 1) {
                                                            $tt = 'Đang chờ';
                                                        }if ($i == 2) {
                                                            $tt = 'Đã gửi đi';
                                                        }if ($i == 3) {
                                                            $tt = 'Hoàn tất';
                                                        }if ($i == 4) {
                                                            $tt = 'Từ chối';
                                                        }
                                                        if ($i == $status) {
                                                            $selected = "selected='selected'";
                                                        }else{
                                                            $selected = "";
                                                        }
                                                        echo "<option disabled $selected value='$i'>$tt</option>";
                                                    }
                                                @endphp
                                            </select>
                                        </div>
                                        <div class="form-group">    
                                                <table width="100%" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <th>Tên sản phẩm</th>
                                                    <th>Số lượng </th>
                                                    <th>Đơn giá</th>
                                                    <th>Giá khuyến mãi</th>
                                                </thead>
                                                <tbody>
                                            @foreach($objProducts as $arProducts)
                                                    <tr class="odd gradeX">
                                                        <td>
                                                            {{$arProducts->name }}
                                                        </td>
                                                        <td>
                                                            {{$arProducts->quantity }}
                                                        </td>
                                                        <td>
                                                            {{$arProducts->unit_price }}.000 VNĐ
                                                        </td>
                                                        <td>
                                                            {{$arProducts->promotion_price }}.000 VNĐ
                                                        </td>
                                                    </tr>
                                            @endforeach
                                                </tbody>
                                            </table>
                                            <h4 style="float:right;color:green">Tổng tiền : {{ $objOrder->total }}.000 VNĐ</h4><br>
                                        </div>                               
                                </div>                             
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                </div>
            </div>
        <!-- end page-wrapper -->
 @stop