@extends('templates.admin.master')
@section('main-content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Quản lý sản phẩm</h1>
                </div>
                <!-- /.col-lg-12 -->
                @if(Session::has('msg'))
                    <script> alert('{{ Session::get('msg') }}')</script>
                @endif
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('admin.product.add')}}" class="fa fa-plus" style="font-size:20px;"> Thêm sản phẩm</a>
                        </div>
                        <style type="text/css">
                            .no-sort::after { display: none!important; }
                            .no-sort { pointer-events: none!important; cursor: default!important; }
                        </style>
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <form action="{{ route('admin.product.destroy')}}" method="post">
                                    {{ csrf_field()}}
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th class="no-sort">

                                        <th class="no-sort">Tên sản phẩm</th>
                                        <th class="no-sort">Danh mục</th>
                                        <th>Giá</th>
                                        <th class="no-sort">Hình ảnh</th>
                                        <th class="no-sort">Hành động</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($objProducts as $arProducts)
                                    @php
                                        $urlPic = Storage::url('app/files/' . $arProducts->images);
                                    @endphp
                                    <tr class="odd gradeX">
                                        <td width="2%" >
                                            <input type="checkbox" name="delete[]" value="{{ $arProducts->pid }}" />
                                        </td>
                                        <td>{{ $arProducts->p_name }}</td>
                                        <td>{{ $arProducts->cat_name }}</td>
                                        <td>{{ $arProducts->unit_price }}.000 VNĐ</td>
                                        <td class="center"><center><img src="{{ $urlPic }}" width="130px" height="100px"></center></td>
                                        <td width="16%">
                                            <a href="{{ route('admin.product.edit',$arProducts->pid)}}" class="btn btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                            <a href="{{ route('admin.product.temp',$arProducts->pid)}}"  class="btn btn-warning"><i class="fa fa-trash"></i> Xóa tạm</a>
                                        </td>
                                    </tr>
                                @endforeach  
                                </tbody>
                            </table>
                            </i><input type="submit" value="Xóa vĩnh viễn" class="btn btn-danger"  onclick="return confirm('Xác nhận xóa các sản phẩm này!?')" >
                        </form>
                          <a style="float: right;font-size:20px" href="{{ route('admin.product.trash')}}" class="fa fa-trash"> Thùng rác</a>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        <!-- /#page-wrapper -->
@stop
