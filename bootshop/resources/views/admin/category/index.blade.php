@extends('templates.admin.master')
@section('main-content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Quản lý danh mục</h1>
                </div>
                <!-- /.col-lg-12 -->
                @if(Session::has('msg'))
                    <script> alert('{{ Session::get('msg') }}')</script>
                @endif
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('admin.category.add')}}" class="fa fa-plus" style="font-size:20px;"> Thêm mới</a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th><center>Tên thể loại</center></th>
                                        <th><center>Danh mục</center></th>
                                        <th><center>Ngày tạo</center></th>
                                        <th><center>Hành động</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($objCat as $arCat)
                                        @php
                                            if ($arCat->parent_id == 1) {
                                                $parent = "Quần áo nam";
                                            }if($arCat->parent_id == 2){
                                                $parent = "Quần áo nữ";
                                            }if($arCat->parent_id == 3){
                                                $parent = "Giày";
                                            }
                                        @endphp
                                    <tr class="odd gradeX">
                                        <td width="5%">{{ $arCat->id }}</td>
                                        <td><center>{{ $arCat->name }}</center></td>
                                        <td><center>{{ $parent }}</center></td>
                                        <td><center>{{ $arCat->created_at }}</center></td>
                                        <td width="22%">
                                            <a href="" class="btn btn-success"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                            <a href="{{ route('admin.category.edit',$arCat->id)}}" class="btn btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                            <a href="{{ route('admin.category.del',$arCat->id)}}" onclick="return confirm('Bạn có chắc muốn xóa danh mục này?')" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $objCat->links()}}
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        <!-- /#page-wrapper -->
@stop