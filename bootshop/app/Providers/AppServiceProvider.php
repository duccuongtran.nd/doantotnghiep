<?php

namespace App\Providers;
use App\Model\Category;
use App\Model\Slide;
use Cart;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::Share('publicUrl',getenv('PUBLIC_URL'));
        View::Share('adminUrl',getenv('ADMIN_URL'));
        View::Share('ImagesPath', getenv('IMAGES_PATH'));
        View::Share('CatSidebar', Category::all());
        View::Share('arSlide', Slide::limit(4)->get());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
