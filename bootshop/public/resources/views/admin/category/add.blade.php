@extends('templates.admin.master')
@section('main-content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Thêm danh mục</h1>
                </div>
                @if(Session::has('msg'))
                    <script> alert('{{ Session::get('msg') }}')</script>
                @endif
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('admin.category.index')}}" class="fa fa-mail-reply" style="font-size:20px;"> Quay lại</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form  action="{{ route('admin.category.add')}}" role="form" method="POST">
                                        {{ csrf_field()}}
                                        <div class="form-group">
                                            <label>Tên thể loại</label>
                                            <input class="form-control" name="category">        
                                        </div>
                                        @if ($errors->has('category'))
                                        <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            {{ array_first($errors->get('category')) }}
                                        </div>
                                        
                                        @endif
                                        <div class="form-group">
                                            <label>Thuộc danh mục</label>
                                                <select name="pid" class="form-control">
                                                    <option value="1">Quần áo nam</option>
                                                    <option value="2">Quần áo nữ</option>
                                                    <option value="3">Giày</option>
                                                </select>       
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" value="Thêm" class="btn btn-primary">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        <!-- /#page-wrapper -->
@stop