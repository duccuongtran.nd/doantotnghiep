<?php

namespace App\Http\Controllers\Shop;
use App\Model\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index(Request $request){
    	
    	$FeatureProducts = Product::where('trash','=','1')->orderBy('id','DESC')->limit(4)->get();
    	$FeatureProducts2 = Product::where('trash','=','1')->orderBy('views','DESC')->limit(4)->get();
    	$LatestProducts = Product::where('trash','=','1')->orderBy('id','ASC')->limit(9)->get();
    	return view('shop.index.index',compact('FeatureProducts','FeatureProducts2','LatestProducts'));
    }
}
