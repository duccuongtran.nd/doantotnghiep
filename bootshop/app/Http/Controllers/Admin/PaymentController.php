<?php

namespace App\Http\Controllers\Admin;
use App\Model\Payment;
use App\Http\Requests\PaymentRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    public function index(){
    	$objPayments = Payment::paginate(5);
    	return view('admin.payment.index',compact('objPayments'));
    }
    public function getAdd(){
    	return view('admin.payment.add');
    }
    public function postAdd(PaymentRequest $request){
        $payment = $request->payment;
        $arItem = array(
        'name' => $payment,
        );
        if(Payment::insert($arItem)){
            $request->session()->flash('msg','Thêm thành công!');
            return redirect()->route('admin.payment.index');
        }else{
            $request->session()->flash('msg','Đã xảy ra lỗi!');
            return redirect()->route('admin.payment.add');
        }
    }
    public function getEdit($id){
    	$objPayment = Payment::findOrFail($id);
      return view('admin.payment.edit',compact('objPayment'));
    }
    public function postEdit($id,PaymentRequest $request){
        $objItem= Payment::find($id);
        $objItem->name =$request->payment;
        if($objItem->update()){
            $request->session()->flash('msg','Sửa thành công');
            return redirect()->route('admin.payment.index');
        }else{
            $request->session()->flash('msg','Đã xảy ra lỗi!');
            return redirect()->route('admin.payment.index');
        }
    }
    public function del($id, Request $request){
        $objItem = Payment::FindOrFail($id);
        if($objItem->delete()){
          $request->session()->flash('msg','Xóa thành công');
          return redirect()->route('admin.payment.index');
          }else{
          $request->session()->flash('msg','Đã xảy ra lỗi!');
          return redirect()->route('admin.payment.index');
        }
    }
}
