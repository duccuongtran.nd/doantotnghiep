@extends('templates.admin.master')
@section('main-content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Quản lý Sliders</h1>
                </div>
                <!-- /.col-lg-12 -->
                @if(Session::has('msg'))
                    <script> alert('{{ Session::get('msg') }}')</script>
                @endif
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('admin.slide.add')}}" class="fa fa-plus" style="font-size:20px;"> Thêm mới</a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th><center>Link</center></th>
                                        <th><center>Picture</center></th>
                                        <th><center>Action</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($objSlide as $arSlide)
                                        @php
                                            $urlPic = Storage::url('app/files/' . $arSlide->picture);
                                        @endphp
                                    <tr class="odd gradeX">
                                        <td width="5%">{{ $arSlide->id }}</td>
                                        <td><center>{{ $arSlide->link }}</center></td>
                                        <td><center><img src="{{ $urlPic }}" width="800px" height="200px"></center></td>
                                        <td width="17%">
                                            <a href="{{ route('admin.slide.edit',$arSlide->id)}}" class="btn btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                            <a href="{{ route('admin.slide.del',$arSlide->id)}}" onclick="return confirm('Bạn có chắc muốn xóa ảnh này?')" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        <!-- /#page-wrapper -->
@stop