<?php

namespace App\Http\Controllers\Admin;
use App\Model\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CouponRequest;
use App\Http\Requests\EditCouponRequest;
class CouponController extends Controller
{
    public function index(){
    	$objCoupons = Coupon::paginate(10);
    	return view('admin.coupon.index',compact('objCoupons'));
    }
    public function getAdd(){
    	return view('admin.coupon.add');
    }
    public function postAdd(CouponRequest $request){
        $name = $request->name;
        $st = $request->st;
        $percent = $request->percent;
        $arItem = array(
        	'name' => $name,
        	'st' => $st,
        	'percent' => $percent,
        );
        if(Coupon::insert($arItem)){
            $request->session()->flash('msg','Thêm thành công!');
            return redirect()->route('admin.coupon.index');
        }else{
            $request->session()->flash('msg','Đã xảy ra lỗi!');
            return redirect()->route('admin.coupon.add');
        }
    }
    public function getEdit($id){
    	$objCoupon = Coupon::findOrFail($id);
      return view('admin.coupon.edit',compact('objCoupon'));
    }
    public function postEdit($id,EditCouponRequest $request){
        $objItem= Coupon::findOrFail($id);
        $objItem->name =$objItem->name;
        $objItem->st =$request->st;
        $objItem->percent =$request->percent;

        if($objItem->update()){
            $request->session()->flash('msg','Sửa thành công');
            return redirect()->route('admin.coupon.index');
        }else{
            $request->session()->flash('msg','Đã xảy ra lỗi!');
            return redirect()->route('admin.coupon.index');
        }
    }
    public function del($id, Request $request){
        $objItem = Coupon::FindOrFail($id);
        if($objItem->delete()){
          $request->session()->flash('msg','Xóa thành công');
          return redirect()->route('admin.coupon.index');
          }else{
          $request->session()->flash('msg','Đã xảy ra lỗi!');
          return redirect()->route('admin.coupon.index');
        }
    }
}
