@extends('templates.admin.master')
@section('main-content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Welcome</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            @if(Session::has('msg'))
                <script> alert('{{ Session::get('msg') }}')</script>
            @endif
            <!-- /.row -->
            <center>
            <h1 class="welcome">
                @if(isset(Auth::user()->fullname))
                    {{Auth::user()->fullname}}
                @endif
            </h1>
            </center>
            <style type="text/css">
                .welcome {
                    font: bold 80px/1 "Helvetica Neue", Helvetica, Arial, sans-serif;
                    text-shadow: 0 1px 0 #ccc, 0 2px 0 #c9c9c9, 0 3px 0 #bbb, 0 4px 0 #b9b9b9, 0 5px 0 #aaa, 0 6px 1px rgba(0,0,0,.1), 0 0 5px rgba(0,0,0,.1), 0 1px 3px rgba(0,0,0,.3), 0 3px 5px rgba(0,0,0,.2), 0 5px 10px rgba(0,0,0,.25), 0 10px 10px rgba(0,0,0,.2), 0 20px 20px rgba(0,0,0,.15);
                    margin-bottom: 20px;
                }
            </style>
            <!-- /.row -->       
        <!-- /#page-wrapper -->
@stop
