<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditCouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'st' => 'required|numeric',
            'percent' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.min' => 'Mã giảm phải là 6 kí tự', 
            'name.max' => 'Mã giảm phải là 6 kí tự', 
            'st.required' => 'Trường này không được bỏ trống',
            'st.numeric' => 'Số lượng phải là số', 
            'percent.required' => 'Trường này không được bỏ trống', 
        ];
    }
}
