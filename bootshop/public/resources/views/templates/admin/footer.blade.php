    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ $adminUrl }}/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ $adminUrl }}/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ $adminUrl }}/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{ $adminUrl }}/vendor/raphael/raphael.min.js"></script>
    <script src="{{ $adminUrl }}/vendor/morrisjs/morris.min.js"></script>
    <script src="{{ $adminUrl }}/data/morris-data.js"></script>
    
    <!-- DataTables JavaScript -->
    <script src="{{ $adminUrl }}/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="{{ $adminUrl }}/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="{{ $adminUrl }}/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ $adminUrl }}/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
            
        });
    });
    </script>

</body>

</html>