<script type="text/javascript" src="{{ $publicUrl }}/js/ccscript.js"></script>
<script type="text/javascript">
	document.getElementById("qtyCart").innerHTML = {{Cart::count()}};
  document.getElementById("qty2").innerHTML = {{Cart::count()}};
</script>
<table class="table table-bordered">
      <thead>
        <tr>
          <th>Tên sản phẩm</th>
          <th>Số lượng/Cập nhật</th>
		      <th>Đơn giá</th>
          <th>Tổng tiền</th>
		</tr>
      </thead>
      <tbody>
      <form action="" method="GET">
      	<input type="hidden" id="_token" name="_token" value="{{ csrf_token()}}">
      	@foreach($updateCart as $arCart)
        <tr>
          <td>{{ $arCart->name }}</td>
		  <td>
			<div class="input-append">
			<input class="span1 qty" style="max-width:34px" placeholder="1" value="{{ $arCart->qty }}" id="appendedInputButtons" size="16" type="text">
			<a class="btn update" id="{{ $arCart->rowId }}" ><i class="icon-refresh"></i></a>
			<a href="{!! route('shop.shop.remove',['id'=>$arCart->rowId])!!}" class="btn btn-danger"><i class="icon-remove icon-white"></i></a>				
			</div>
		  </td>
          <td>{{ $arCart->price }}.000 VNĐ</td>
          <td>{{ $arCart->price * $arCart->qty }}$</td>
        </tr>
		@endforeach
	</form>
        <tr>
          <td colspan="3" style="text-align:right">Tổng tiền:	</td>
          <td id="totalc">{{ Cart::subtotal()}}0 VNĐ</td>
        </tr>
		</tbody>
</table>