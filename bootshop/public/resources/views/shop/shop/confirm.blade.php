{{-- {{dd(Session::all())}} --}}
@extends('templates.shop.master')
@section('main-content')
{{-- <div id="mainBody"> --}}
	@if(Cart::count() == 0)
		<script type="text/javascript">
			alert('Giỏ hàng rỗng');
			location.href = '../shop';
		</script>

	@endif
	<div class="container">
	<div class="row">
@include ('templates.shop.sidebar')
	<div class="span9">
	@if(Session::has('msg'))
		<script type="text/javascript">
			alert("{{ Session::get('msg')}}");
		</script>
	@endif
	<div class="well">
		<form method="post" action="{{route('shop.shop.confirm')}}" class="form-horizontal" >
			{{csrf_field()}}
			<h4>Thông tin đơn hàng</h4>
	
			<div class="alert alert-success">
				@php
					if (Session::has('arCustomer')) {
						$Customer = Session::get('arCustomer');
						echo "<p>Họ tên : ".$Customer['name']."</p>";
						echo "<p>Địa chỉ giao hàng : ".$Customer['address']."</p>";
						echo "<p>Số điện thoại: ".$Customer['phone']."</p>";
					} 		
			  	@endphp
			</div>
			<div class="alert alert-info">
			<table style="color:black;border: 1px solid skyblue;" class="table table-bordered">
				<th>Tên sản phẩm</th>
			<th>Số lượng</th>
			<th>Giá</th>
			@foreach($checkCart as $arCheckCart)
				@php
            		$slug = str_slug($arCheckCart->name);
        			$url = route('shop.shop.detail',['slug'=>$slug,'id'=>$arCheckCart->id]);
            	@endphp
			<tr>
				<td>
					<a href="{{$url}}" style="color:red">{{$arCheckCart->name}}</a>
				</td>
				<td>
					{{$arCheckCart->qty}}
				</td>
				<td>
					{{$arCheckCart->price}}.000 VNĐ
				</td>
			</tr>
			@endforeach
				<tr>
					<td  colspan="6" style="text-align:right">Tổng tiền : {{Cart::subtotal()}}.000 VNĐ</td>
					@if(Session::has('coupon'))
						<tr>
							<td  colspan="6" style="text-align:right">Giảm còn : {{Cart::subtotal(0,'','')*Session::get('coupon')}}.000 VNĐ</td>
						</tr>
					@endif
				</tr>
			</table>
			</div>
			<div class="alert alert-error">
			  	<h5 style="color:black">Hình thức thanh toán</h5>
			  	@php
			  		if (Session::has('payment')) {
			  			$check = Session::get('payment');
			  			if ($check == 1) {
			  				echo "<p>Giao hàng tận nhà</p>";
			  			}else{
			  				echo "<p>Đã thanh toán thông qua Paypal</p>";
			  			}
			  		}else{
			  			
			  		}
			  	@endphp
			</div>
			<div class="control-group">
				<div class="controls">	
					<input class="btn btn-large btn-success" type="submit" value="Xác nhận" />
				</div>
			</div>		
		</form>
	</div>
</div>
</div>
</div>
@stop