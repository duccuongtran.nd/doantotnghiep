<div id="carouselBlk">
    <div id="myCarousel" class="carousel slide">
        <div class="carousel-inner">
            <div class="item active">
                <div class="container">
                    <a href="{{route('shop.shop.register')}}"><img src="{{ $ImagesPath}}/activeslide.jpg" alt="SPECIAL OFFER"/></a>
                    <div class="carousel-caption">
                    </div>
                </div>
            </div>
            @foreach($arSlide as $arSlide )
            <div class="item">
                <div class="container">
                    <a href="{{route('shop.shop.register')}}"><img src="{{ $ImagesPath }}/{{$arSlide->picture}}" alt=""/></a>

                </div>
            </div>
            @endforeach
        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div>
</div>