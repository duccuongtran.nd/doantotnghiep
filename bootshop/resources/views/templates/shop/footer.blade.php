<!-- Footer ================================================================== -->
	<div  id="footerSection">
	<div class="container">
		<div class="row">
			<div class="span3">
				<h5>PAGE</h5>
				<a href="/">TRANG CHỦ</a>
				<a href="{{route('shop.shop.shop')}}">SẢN PHẨM</a>
				<a href="{{route('shop.shop.cart')}}">GIỎ HÀNG</a>
				<a href="/">HÌNH THỨC THANH TOÁN</a>
			 </div>
			<div class="span3">
				<h5>THÔNG TIN</h5>
				<a href="{{route('shop.shop.contact')}}">LIÊN HỆ</a>  
				<a href="{{route('shop.shop.register')}}">ĐĂNG KÝ</a>  
				<a href="/">CHÍNH SÁCH MUA BÁN</a> 
				<a href="/">FAQ</a>
			 </div>
			<div class="span3">
				<h5>ƯU ĐÃI</h5>
				<a href="{{route('shop.shop.shop')}}">SẢN PHẨM MỚI</a> 
				<a href="{{route('shop.shop.offer')}}">KHUYẾN MÃI ĐẶC BIỆT</a>  
				<a href="/">NHÀ SẢN XUẤT</a> 
				<a href="/">NHÀ CUNG CẤP</a> 
			 </div>
			<div id="socialMedia" class="span3 pull-right">
				<h5>MẠNG XÃ HỘI </h5>
				<a href="#"><img width="60" height="60" src="{{ $publicUrl }}/themes/images/facebook.png" title="facebook" alt="facebook"/></a>
				<a href="#"><img width="60" height="60" src="{{ $publicUrl }}/themes/images/twitter.png" title="twitter" alt="twitter"/></a>
				<a href="#"><img width="60" height="60" src="{{ $publicUrl }}/themes/images/youtube.png" title="youtube" alt="youtube"/></a>
			 </div> 
		 </div>
		<p class="pull-right">&copy; Bootshop</p>
	</div><!-- Container End -->
	</div>
<script type="text/javascript">
	$(function(){
	    $('.add-to-cart').click(function(){
	        var $_this = $(this);
	        var itemImg = $(this).closest('.thumbnail').find('img').eq(0);
	         flyToElement($(itemImg), $('.cart_anchor'));
	    });
	});
</script>
<!-- Placed at the end of the document so the pages load faster ============================================= -->
	<script src="{{ $publicUrl }}/themes/js/jquery.js" type="text/javascript"></script>
	<script src="{{ $publicUrl }}/themes/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="{{ $publicUrl }}/themes/js/google-code-prettify/prettify.js"></script>
	
	<script src="{{ $publicUrl }}/themes/js/bootshop.js"></script>
    <script src="{{ $publicUrl }}/themes/js/jquery.lightbox-0.5.js"></script>
	
	<!-- Themes switcher section ============================================================================================= -->
<div id="secectionBox">
<link rel="stylesheet" href="{{ $publicUrl }}/themes/switch/themeswitch.css" type="text/css" media="screen" />
<script src="{{ $publicUrl }}/themes/switch/theamswitcher.js" type="text/javascript" charset="utf-8"></script>
	
</div>
<a href="{{route('shop.shop.cart')}}"><span id="themesBtn" class="cart_anchor"><b id="qty2" class="badge badge-warning pull-left">{{Cart::count()}}</b> </span></a>
</body>
</html>