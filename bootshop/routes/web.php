<?php
Route::get('storage/{filename}', function ($filename)
{
    $path = storage_path('public/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});




Route::pattern('slug','.*');
Route::pattern('id','([0-9]*)');
Route::pattern('idc','(.+)');
Route::group(['namespace'=>'shop'],function(){
	Route::get('','IndexController@index');
	Route::get('shop',[
			'uses' => 'ShopController@shop',
			'as' => 'shop.shop.shop'
		]);
	Route::get('{slug}-{id}.html',[
			'uses' => 'ShopController@detail',
			'as' => 'shop.shop.detail'
		]);
	/*Route::get('tin-tuc-khuyen-mai.html',[
			'uses' => 'NewsController@news',
			'as' => 'shop.shop.news'
		]);
	Route::get('{slug}-{id}.html',[
			'uses' => 'NewsController@news',
			'as' => 'shop.shop.detail'
		]);*/
	Route::get('{slug}-{id}',[
			'uses' => 'ShopController@category',
			'as' => 'shop.shop.category'
		]);
	Route::get('specials-offer.html',[
			'uses' => 'ShopController@offers',
			'as' => 'shop.shop.offer'
		]);

	Route::post('search',[
			'uses' => 'ShopController@postSearch',
			'as' => 'shop.shop.search'
		]);
	Route::get('register',[
			'uses' => 'ShopController@register',
			'as' => 'shop.shop.register'
		]);
	Route::post('register',[
			'uses' => 'ShopController@postRegister',
			'as' => 'shop.shop.register'
		]);
	Route::get('cart',[
			'uses' => 'ShopController@cart',
			'as' => 'shop.shop.cart'
		]);
	Route::get('cart/addItem/{id}','ShopController@addItem');

	Route::get('cart/removeItem/{idc}',[
			'uses' => 'ShopController@delItem',
			'as' => 'shop.shop.remove'
		]);
	Route::get('cart/update/{idc}/{qty}',[
			'uses' => 'ShopController@updateCart',
			'as' => 'shop.shop.update'
		]);
	Route::get('order/step1',[
			'uses' => 'ShopController@step1',
			'as' => 'shop.shop.step1'
		]);
	Route::post('order/step1',[
			'uses' => 'ShopController@postStep1',
			'as' => 'shop.shop.step1'
		]);
	Route::get('order/step2',[
			'uses' => 'ShopController@step2',
			'as' => 'shop.shop.step2'
		]);
	Route::post('order/step2',[
			'uses' => 'ShopController@postStep2',
			'as' => 'shop.shop.step2'
		]);
	Route::get('order/thanh-toan.html',[
			'uses' => 'ShopController@payment',
			'as' => 'shop.shop.payment'
		]);
	Route::post('order/thanh-toan.html',[
			'uses' => 'ShopController@postPayment',
			'as' => 'shop.shop.payment'
		]);
	Route::get('order/thanh-toan.html/{coupon}','ShopController@addCoupon');
	Route::get('order/success',[
			'uses' => 'ShopController@checkout',
			'as' => 'shop.shop.checkout'
		]);
	Route::get('order/xac-nhan-don-hang.html',[
			'uses' => 'ShopController@xacnhan',
			'as' => 'shop.shop.confirm'
		]);
	Route::post('order/xac-nhan-don-hang.html',[
			'uses' => 'ShopController@postXacnhan',
			'as' => 'shop.shop.confirm'
		]);
	Route::get('contact',[
			'uses' => 'ShopController@contact',
			'as' => 'shop.shop.contact'
		]);
	Route::post('contact',[
			'uses' => 'ShopController@postContact',
			'as' => 'shop.shop.contact'
		]);
	Route::get('demo',[
			'uses' => 'ShopController@demo',
			'as' => 'shop.shop.demo'
		]);
});

// Admin Route
Route::group(['namespace'=>'admin','prefix' => 'admincp','middleware' => 'auth'],function(){
	Route::get('',[
			'uses' => 'IndexController@index',
			'as' => 'admin.index.index'
		]);
	// Products Management
	Route::group(['prefix'=>'product','middleware' => 'permission:1|2'],function(){
		Route::get('',[
				'uses' => 'ProductController@index',
				'as' => 'admin.product.index'
			]);
		Route::get('add',[
				'uses' => 'ProductController@getAdd',
				'as' => 'admin.product.add'
			]);
		Route::post('add',[
				'uses' => 'ProductController@postAdd',
				'as' => 'admin.product.add'
			]);	
		Route::get('edit/{id}',[
				'uses' => 'ProductController@getEdit',
				'as' => 'admin.product.edit'
			]);
		Route::post('edit/{id}',[
				'uses' => 'ProductController@postEdit',
				'as' => 'admin.product.edit'
			]);
		Route::get('trash',[
				'uses' => 'ProductController@trash',
				'as' => 'admin.product.trash'
			]);
		Route::get('temp/{id}',[
				'uses' => 'ProductController@temp',
				'as' => 'admin.product.temp'
			]);
		Route::get('restore/{id}',[
				'uses' => 'ProductController@restore',
				'as' => 'admin.product.restore'
			]);
		Route::post('destroy',[
				'uses' => 'ProductController@destroy',
				'as' => 'admin.product.destroy'
			]);
	});
	// Category Management
	Route::group(['prefix'=>'category','middleware' => 'permission:1|2'],function(){
		Route::get('',[
				'uses' => 'CategoryController@index',
				'as' => 'admin.category.index'
			]);
		Route::get('add',[
				'uses' => 'CategoryController@getAdd',
				'as' => 'admin.category.add'
			]);
		Route::post('add',[
				'uses' => 'CategoryController@postAdd',
				'as' => 'admin.category.add'
			]);	
		Route::get('edit/{id}',[
				'uses' => 'CategoryController@getEdit',
				'as' => 'admin.category.edit'
			]);
		Route::post('edit/{id}',[
				'uses' => 'CategoryController@postEdit',
				'as' => 'admin.category.edit'
			]);
		Route::get('del/{id}',[
				'uses' => 'CategoryController@del',
				'as' => 'admin.category.del'
			]);
	});
	// Payment Management
	Route::group(['prefix'=>'payment','middleware' => 'permission:1|2'],function(){
		Route::get('',[
				'uses' => 'PaymentController@index',
				'as' => 'admin.payment.index'
			]);
		Route::get('add',[
				'uses' => 'PaymentController@getAdd',
				'as' => 'admin.payment.add'
			]);
		Route::post('add',[
				'uses' => 'PaymentController@postAdd',
				'as' => 'admin.payment.add'
			]);	
		Route::get('edit/{id}',[
				'uses' => 'PaymentController@getEdit',
				'as' => 'admin.payment.edit'
			]);
		Route::post('edit/{id}',[
				'uses' => 'PaymentController@postEdit',
				'as' => 'admin.payment.edit'
			]);
		Route::get('del/{id}',[
				'uses' => 'PaymentController@del',
				'as' => 'admin.payment.del'
			]);
	});
	// Coupons Controller
	Route::group(['prefix'=>'coupons','middleware' => 'permission:1|2'],function(){
		Route::get('',[
				'uses' => 'CouponController@index',
				'as' => 'admin.coupon.index'
			]);
		Route::get('add',[
				'uses' => 'CouponController@getAdd',
				'as' => 'admin.coupon.add'
			]);
		Route::post('add',[
				'uses' => 'CouponController@postAdd',
				'as' => 'admin.coupon.add'
			]);	
		Route::get('edit/{id}',[	
				'uses' => 'CouponController@getEdit',
				'as' => 'admin.coupon.edit'
			]);
		Route::post('edit/{id}',[
				'uses' => 'CouponController@postEdit',
				'as' => 'admin.coupon.edit'
			]);
		Route::get('del/{id}',[
				'uses' => 'CouponController@del',
				'as' => 'admin.coupon.del'
			]);
	});
	// Slide Management
	Route::group(['prefix'=>'slide','middleware' => 'permission:1|2'],function(){
		Route::get('',[
				'uses' => 'SlideController@index',
				'as' => 'admin.slide.index'
			]);
		Route::get('add',[
				'uses' => 'SlideController@getAdd',
				'as' => 'admin.slide.add'
			]);
		Route::post('add',[
				'uses' => 'SlideController@postAdd',
				'as' => 'admin.slide.add'
			]);	
		Route::get('edit/{id}',[
				'uses' => 'SlideController@getEdit',
				'as' => 'admin.slide.edit'
			]);
		Route::post('edit/{id}',[
				'uses' => 'SlideController@postEdit',
				'as' => 'admin.slide.edit'
			]);
		Route::get('del/{id}',[
				'uses' => 'SlideController@del',
				'as' => 'admin.slide.del'
			]);
	});
	// Contact Management
	Route::group(['prefix'=>'contact','middleware' => 'permission:1|2'],function(){
		Route::get('',[
				'uses' => 'ContactController@index',
				'as' => 'admin.contact.index'
			]);
		Route::get('view/{id}',[
				'uses' => 'ContactController@getView',
				'as' => 'admin.contact.view'
			]);
		Route::get('del/{id}',[
				'uses' => 'ContactController@del',
				'as' => 'admin.contact.del'
			]);
	});
	// Order Management
	Route::group(['prefix'=>'order','middleware' => 'permission:1'],function(){
		Route::get('',[
				'uses' => 'BillController@index',
				'as' => 'admin.order.index'
			]);
		Route::get('view/{id}',[
				'uses' => 'BillController@getView',
				'as' => 'admin.order.view'
			]);
		Route::post('view/{id}',[
				'uses' => 'BillController@postView',
				'as' => 'admin.order.view'
			]);
		Route::get('del/{id}',[
				'uses' => 'BillController@del',
				'as' => 'admin.order.del'
			]);
	});
	// User Management
	Route::group(['prefix'=>'users','middleware' => 'permission:1'],function(){
		Route::get('',[
				'uses' => 'UserController@index',
				'as' => 'admin.user.index'
			]);
		
		Route::get('edit/{id}',[
				'uses' => 'UserController@getEdit',
				'as' => 'admin.user.edit'
			]);
		Route::post('edit/{id}',[
				'uses' => 'UserController@postEdit',
				'as' => 'admin.user.edit'
			]);
		Route::get('del/{id}',[
				'uses' => 'UserController@del',
				'as' => 'admin.user.del'
			]);
	});
	Route::group(['prefix'=>'checkorder'],function(){
		Route::get('',[
				'uses' => 'BillController@indexcheck',
				'as' => 'admin.checkorder.index'
			]);
		Route::get('info',[
				'uses' => 'BillController@info',
				'as' => 'admin.checkorder.info'
			]);
		Route::post('info/{id}',[
				'uses' => 'BillController@postInfo',
				'as' => 'admin.checkorder.editInfo'
			]);
	});

});
Route::group(['namespace' =>'Auth'],function(){
	Route::get('login',[
			'uses' => 'AuthController@getLogin',
			'as' => 'auth.auth.login'
		]);
	Route::post('login',[
			'uses' => 'AuthController@postLogin',
			'as' => 'auth.auth.login'
		]);
	Route::get('logout',[
			'uses' => 'AuthController@logout',
			'as' => 'auth.auth.logout'
		]);
});