@extends('templates.admin.master')
@section('main-content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Chỉnh sửa User</h1>
                </div>
                @if(Session::has('msg'))
                    <script> alert('{{ Session::get('msg') }}')</script>
                @endif
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('admin.user.index')}}" class="fa fa-mail-reply" style="font-size:20px;"> Quay lại</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form  action="{{ route('admin.user.edit',$objUser->id) }}" role="form" method="POST">
                                        {{ csrf_field()}}
                                        <div class="form-group">
                                            <label>Username</label>
                                            <h5> {{$objUser->username }}</h5>
                                            {{ array_first($errors->get('username')) }}    
                                        </div>
                                        <div class="form-group">
                                            <label>Role</label>
                                            <select class="form-control" name="role">
                                                @php
                                                    for ($i=1; $i <= 3 ; $i++) { 
                                                        if ($i == 1) {
                                                            $level = "Administrator";
                                                        }if($i == 2){
                                                            $level = "Moderator";
                                                        }if($i == 3){
                                                            $level = "Customer";
                                                        }
                                                        if ($i == $objUser->role) {
                                                            $selected = "selected='selected'";
                                                        }else{
                                                            $selected ="";
                                                        }
                                                        echo "<option $selected value='$i'>$level</option>";
                                                    }

                                                @endphp
                                                    
                                            </select> 
                                        </div>
                                        <div class="form-group">
                                            <label>Full name</label>
                                            <input class="form-control" name="fullname" value="{{$objUser->fullname }}">
                                            {{ array_first($errors->get('fullname')) }}       
                                        </div>
                                        <div class="form-group">
                                            <label>Passwold </label>
                                            <input type="password" class="form-control" name="password" value="">
                                            {{ array_first($errors->get('password')) }}       
                                        </div>
                                        <div class="form-group">
                                            <label>Old Password</label>
                                        <input type="password" class="form-control" name="oldpassword" value="">
                                            {{ array_first($errors->get('oldpassword')) }}       
                                        </div>
                                        <div class="form-group">
                                            <label>Phone</label>
                                            <input class="form-control" name="phone" value="{{$objUser->phone }}">
                                            {{ array_first($errors->get('phone')) }}    
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" name="email" value="{{$objUser->email }}">
                                            {{ array_first($errors->get('email')) }}    
                                        </div>
                                        <div class="form-group">
                                            <label>Address</label>
                                            <textarea class="form-control" name="address">{{$objUser->address }}</textarea>
                                            {{ array_first($errors->get('address')) }}    
                                        </div>
                                        <div class="form-group">
                                            <label>Note</label>
                                            <textarea class="form-control" name="note">{{$objUser->note }}</textarea>
                                            {{ array_first($errors->get('note')) }}    
                                        </div>
                                        
                                        <div class="form-group">
                                            <input type="submit" value="Edit" class="btn btn-primary">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        <!-- /#page-wrapper -->
@stop