@extends('templates.shop.master')
@section('main-content')
{{-- <div id="mainBody"> --}}
	@if(Cart::count() == 0)
		<script type="text/javascript">
			alert('Giỏ hàng rỗng');
			location.href = '../shop';
		</script>

	@endif
	<div class="container">
	<div class="row">
@include ('templates.shop.sidebar')
	<div class="span9">
	@if(Session::has('msg'))
		<script type="text/javascript">
			alert("{{ Session::get('msg')}}");
		</script>
	@endif
	<div class="well">
		<h5 style="color:#ffc439">Lưu ý</h5>
		<p style="color:red">Vui lòng điền chính xác thông tin trước khi Xác nhận mua hàng !</p>
		@if(!isset(Auth::user()->username))
		<form method="post" action="{{route('shop.shop.step2')}}" class="form-horizontal" >
			{{csrf_field()}}
			<h4>Thông tin cá nhân của bạn</h4>
			<div class="control-group">
				<label class="control-label">Họ và tên <sup>*</sup></label>
				<div class="controls">
				  <input name="fullname" value="{{ old('fullname') }}" type="text" id="inputFname1" placeholder="Họ và tên nên có ít nhất 10 kí tự">
				  {{ array_first($errors->get('fullname')) }}
				</div>
			 </div>
			<div class="control-group">
			<label class="control-label">Số điện thoại <sup>*</sup></label>
				<div class="controls">
				  <input name="phone" value="{{ old('phone') }}" type="text" id="input_email" placeholder="Số điện thoại của bạn">
				  {{ array_first($errors->get('phone')) }}
				</div>
		   </div>
		    <div class="control-group">
				<label class="control-label">Email <sup></sup></label>
				<div class="controls">
				  <input name="email" value="{{ old('email') }}" type="text" id="inputLnam" placeholder="Email của bạn">
				  {{ array_first($errors->get('email')) }}
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Địa chỉ và Ghi Chú*<sup></sup></label>
				<div class="controls">
				  	<textarea name="address" placeholder="Địa chỉ nên có ít nhất 10 kí tự">{{ old('address') }}</textarea>
				  	{{ array_first($errors->get('address')) }}
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Lưu ý<sup></sup></label>
				<div class="controls">
				  	<textarea name="note" placeholder="Lưu ý thêm của bạn">{{ old('note') }}</textarea>
				</div>
			</div>
		
			<div class="control-group">
				<div class="controls">	
					<input class="btn btn-large btn-success" type="submit" value="Tiếp tục" />
				</div>
			</div>		
		</form>
		@else
		<form method="post" action="{{route('shop.shop.step2')}}" class="form-horizontal" >
			{{csrf_field()}}
			<h4>Your personal information</h4>
			<div class="control-group">
				<label class="control-label">Họ tên <sup>*</sup></label>
				<div class="controls">
				  <input name="fullname" value="{{Auth::user()->fullname}}" type="text" id="inputFname1" placeholder=" Họ tên ít nhất phải 10 kí tự">
				  {{ array_first($errors->get('fullname')) }}
				</div>
			 </div>
			<div class="control-group">
			<label class="control-label">Số điện thoại <sup>*</sup></label>
				<div class="controls">
				  <input name="phone" value="{{Auth::user()->phone}}" type="text" id="input_email" placeholder="Phone">
				  {{ array_first($errors->get('phone')) }}
				</div>
		   </div>
		    <div class="control-group">
				<label class="control-label">Email <sup></sup></label>
				<div class="controls">
				  <input name="email" value="{{Auth::user()->email}}" type="text" id="inputLnam" placeholder="Email">
				  {{ array_first($errors->get('email')) }}
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Địa chỉ *<sup></sup></label>
				<div class="controls">
				  	<textarea name="address" placeholder="Địa chỉ phải ít nhất 10 kí tự">{{Auth::user()->address}}</textarea>
				  	{{ array_first($errors->get('address')) }}
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Lưu ý thêm<sup></sup></label>
				<div class="controls">
				  	<textarea name="note">{{ old('note') }}</textarea>
				</div>
			</div>	  
			
			<div class="control-group">
				<div class="controls">	
					<input class="btn btn-large btn-success" type="submit" value="Tiếp tục" />
				</div>
			</div>		
		</form>
		@endif
	</div>
	<div class="well">
		<table style="color:green;" class="table table-bordered">
			<th>Tên sản phẩm</th>
			<th>Số lượng</th>
			
			<th>Giá</th>
			@foreach($checkCart as $arCheckCart)
				@php
            		$slug = str_slug($arCheckCart->name);
        			$url = route('shop.shop.detail',['slug'=>$slug,'id'=>$arCheckCart->id]);
            	@endphp
			<tr>
				<td>
					<a href="{{$url}}" style="color:red">{{$arCheckCart->name}}</a>
				</td>
				<td>
					{{$arCheckCart->qty}}
				</td>
				<td>
					{{$arCheckCart->price}} $
				</td>
			</tr>
			@endforeach
			<tr>
				<td  colspan="6" style="text-align:right">Tổng tiền : {{Cart::subtotal()}} $</td>
			</tr>
		</table>
	</div>
	
</div>
</div>
</div>
@stop