$(document).ready(function(){
	$(".update").click(function(){
		var rowId = $(this).attr('id');
		var qty = $(this).parent().parent().find('.qty').val();
		var token = $('#_token').val();

		$.ajax({
			url : 'cart/update/'+rowId+'/'+qty,
			type : 'GET',
			cache :false,
			data : {"_token":token,"id":rowId,"qty":qty},

			success:function(data){
				alert('Giỏ hàng đã được cập nhật !');
				
				$('#show').empty();
                $('#show').html(data)              
			}
		});
	});
});	