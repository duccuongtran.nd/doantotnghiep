@extends('templates.shop.master')
@section('main-content')
{{-- <div id="mainBody"> --}}
@if(Session::has('msg'))
	<script type="text/javascript">
		alert('{{Session::get('msg')}}');
	</script>
@endif
	<div class="container">
	<div class="row">
@include ('templates.shop.sidebar')
@if(isset(Auth::user()->username))
	<script type="text/javascript">
		location.href = 'step2';
	</script>
@endif
	<div class="span9">
    <ul class="breadcrumb">
		
		<li class="active"> Bước 1</li>
    </ul>
	<h3>  SHOPPING CART <a href="{{route('shop.shop.shop')}}" class="btn btn-large pull-right"><i class="icon-arrow-left"></i> Quay lại mua sắm</a></h3>	
	<hr class="soft"/>
	<table class="table table-bordered">
		<tr><th> Tôi đã có tài khoản!  </th></tr>
		 <tr> 
		 <td>
			<form method="post" action="{{route('shop.shop.step1')}}" class="form-horizontal">
				{{csrf_field()}}
				<div class="control-group">
				  <label class="control-label" for="inputUsername">Username</label>
				  <div class="controls">
					<input type="text" name="username" id="inputUsername" placeholder="Username">
				  </div>
				</div>
				<div class="control-group">
				  <label class="control-label" for="inputPassword1">Password</label>
				  <div class="controls">
					<input type="password" name="password" id="inputPassword1" placeholder="Password">
				  </div>
				</div>
				<div class="control-group">
				  <div class="controls">
					<button type="submit" class="btn btn-primary">Đăng nhập</button> OR 
					<a href="{{route('shop.shop.register')}}" class="btn btn-warning">Đăng ký!</a>
				  </div>
				</div>
				<div class="control-group">
					<div class="controls">
					  {{-- <a href="forgetpass.html" style="text-decoration:underline">Forgot password ?</a> --}}
					</div>
				</div>
			</form>
		  </td>
		  </tr>
	</table>			
	<a href="{{route('shop.shop.step2')}}" class="btn btn-success"> Mua hàng không cần tài khoản! <i class="icon-arrow-right"></i> </a>
	
</div>
</div></div>
@stop