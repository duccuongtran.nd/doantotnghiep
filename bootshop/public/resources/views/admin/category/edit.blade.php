@extends('templates.admin.master')
@section('main-content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Chỉnh sửa danh mục</h1>
                </div>
                @if(Session::has('msg'))
                    <script> alert('{{ Session::get('msg') }}')</script>
                @endif
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('admin.category.index')}}" class="fa fa-mail-reply" style="font-size:20px;"> Quay lại</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form  action="{{ route('admin.category.edit',$objCat->id) }}" role="form" method="POST">
                                        {{ csrf_field()}}
                                        <div class="form-group">
                                            <label>Tên thể loại</label>
                                            <input class="form-control" name="category" value="{{$objCat->name }}">        
                                        </div>
                                        @if ($errors->has('category'))
                                        <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            {{ array_first($errors->get('category')) }}
                                        </div>
                                        @endif
                                        <div class="form-group">
                                            <label>Thuộc danh mục</label>
                                            <select name="pid" class="form-control">
                                                @php
                                                    for ($i=1; $i <= 3 ; $i++) {
                                                        if ($i == $objCat->parent_id) {
                                                            $selected = "selected='selected'";
                                                        }else{
                                                            $selected = "";
                                                        }
                                                        if ($i == 1) {
                                                            $level = 'Quần áo nam';
                                                        }elseif ($i == 2) {
                                                            $level = 'Quần áo nữ';
                                                        }else{
                                                            $level = 'Giày';
                                                        }                                           echo "<option {$selected} value='{$i}''>{$level}</option>";
                                                    }
                                                  
                                                @endphp
                                                
                                            </select>

                                        </div>
                                        <div class="form-group">
                                            <input type="submit" value="Chỉnh sửa" class="btn btn-primary">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        <!-- /#page-wrapper -->
@stop