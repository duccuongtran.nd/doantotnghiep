@extends('templates.admin.master')
@section('main-content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Payment</h1>
                </div>
                @if(Session::has('msg'))
                    <script> alert('{{ Session::get('msg') }}')</script>
                @endif
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('admin.payment.index')}}" class="fa fa-mail-reply" style="font-size:20px;"> Back to Payment Page</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form  action="{{ route('admin.payment.add')}}" role="form" method="POST">
                                        {{ csrf_field()}}
                                        <div class="form-group">
                                            <label>Payment Name</label>
                                            <input class="form-control" name="payment">        
                                        </div>
                                        @if ($errors->has('payment'))
                                        <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            {{ array_first($errors->get('payment')) }}
                                        </div>
                                        @endif

                                        <div class="form-group">
                                            <input type="submit" value="Add Payment" class="btn btn-primary">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        <!-- /#page-wrapper -->
@stop