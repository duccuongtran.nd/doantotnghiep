@extends('templates.shop.master')
@section('main-content')
{{-- <div id="mainBody"> --}}
	<div class="container">
	<div class="row">
@include ('templates.shop.sidebar')
	<div class="span9">
	<div class="row">	  
			<div id="gallery" class="span3">
            <a href="{{ $ImagesPath }}/{{$objDetail->images}}" title="Fujifilm FinePix S2950 Digital Camera">
				<img src="{{ $ImagesPath }}/{{$objDetail->images}}" style="width:100%;padding-bottom: 30px" alt="{{$objDetail->name}}"/>
            </a>
			<div id="differentview" class="moreOptopm carousel slide">
                <div class="carousel-inner">
                  <div class="item active">
                   @foreach($ThumbImages as $arThumb)
                   @php
                        $urlThumb = Storage::url('app/' . $arThumb->name);
                    @endphp
                   <a href="{{$urlThumb}}"> <img style="width:29%" src="{{ $urlThumb}}" alt=""/></a>
                   @endforeach
                  </div>
                  <div class="item">
                   @foreach($ThumbImages as $arThumb)
                   @php
                        $urlThumb = Storage::url('app/' . $arThumb->name);
                    @endphp
                   <a href="{{$urlThumb}}"> <img style="width:29%" src="{{ $urlThumb}}" alt=""/></a>
                   @endforeach
                  </div>
                </div>
              <!--  
			  <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
              <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a> 
			  -->
              </div>
			 
			</div>
			<div class="span6">
				<h4 style="color:red"> {{ $objDetail->name}} </h4>
				<hr class="soft"/>
				<form class="form-horizontal qtyFrm">
				  <div class="control-group">
				  	@php
				  		if ($objDetail->promotion_price == 0) {
				  			$price = $objDetail->unit_price;
				  		}else{
				  			$price = $objDetail->promotion_price;
				  		}
				  	@endphp
					<label class="control-label"><span style="color:green;font-size:20px">Đơn giá : <i style="color:black">{{$price}}.000 VNĐ</i></span></label>
					<div class="controls">
					
					  <a href="javascript:;" id="{{$objDetail->id}}" class="btn btn-large btn-warning pull-right addCart" onclick="alert('Thêm thành công!')">Thêm vào giỏ!</a>
					</div>
				  </div>
				</form>
				
				<hr class="soft"/>
				{{-- <h4>100 items in stock</h4>
				<form class="form-horizontal qtyFrm pull-right">
				<div class="control-group">
					<label class="control-label"><span>Color</span></label>
					<div class="controls">
					  <select class="span2">
						  <option>Black</option>
						  <option>Red</option>
						  <option>Blue</option>
						  <option>Brown</option>
						</select>
					</div>
				  </div>
				</form>
				<hr class="soft clr"/> --}}
				<p>
					{{$objDetail->description}}
				</p>
				<a class="btn btn-small pull-right" href="#detail">Xem chi tiết</a>
				<br class="clr"/>
			<a href="#" name="detail"></a>
			<hr class="soft"/>
			</div>
			
			<div class="span9">
            <ul id="productDetail" class="nav nav-tabs">
              <li class="active"><a href="#home" data-toggle="tab">Chi tiết sản phẩm</a></li>
              <li><a href="#profile" data-toggle="tab">Sản phẩm có liên quan</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
              <div class="tab-pane fade active in" id="home">
			  <h4>Product Information</h4>
                	{!! $objDetail->detail !!}
              </div>
		<div class="tab-pane fade" id="profile">
		<div id="myTab" class="pull-right">
		 	<h5>Được đề xuất</h5>
		</div>
		<br class="clr"/>
		<hr class="soft"/>
		<div class="tab-content">
			
			<div class="tab-pane active" id="blockView">
				<ul class="thumbnails">
					@foreach($objRelated as $arRelated)
					@php
						$id = $arRelated->id;
		    			$name = $arRelated->name;
		    			$description = $arRelated->description;
		    			$cutDes = str_limit($description,120,'...');
		    			$images = $arRelated->images;
		    			$unit_price = $arRelated->unit_price;
		    			$promotion_price = $arRelated->promotion_price;
		    			$slug = str_slug($name);
	        			$url = route('shop.shop.detail',['slug'=>$slug,'id'=>$id]);
					@endphp
					<li class="span3">
					  <div class="thumbnail">
						<a href="{{ $url }}"><img src="{{ $ImagesPath }}/{{$images}}" alt="{{ $name }}" style="width:250px;height: 200px" /></a>
						<div class="caption">
						  <h6 style="max-height: 15px;padding-bottom: 20px"><a href="{{ $url }}">{{ $name }}</a></h6>
						  <p> 
							{{ $cutDes }}
						  </p>
						    <h4 style="text-align:center"><a href="javascript:;" id="{{$id}}" class="btn btn-warning addCart">Add to <i class="icon-shopping-cart"></i></a>
							  	@if($promotion_price == 0)
							  		<a class="btn btn-primary" href="#">${{$unit_price}}</a>
							  	@else
							  		<a class="btn btn-danger" style="text-decoration: line-through;" href="#">${{$unit_price}}</a>
							  		<a class="btn btn-primary" href="#">${{$promotion_price}}</a>
							  	@endif
							  </h4>
						</div>
					  </div>
					</li>
					@endforeach
				  </ul>
			<hr class="soft"/>
			</div>
		</div>
				<br class="clr">
					 </div>
		</div>
          </div>

	</div>
</div>
</div> </div>
@stop