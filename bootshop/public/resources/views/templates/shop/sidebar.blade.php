<!-- Sidebar ================================================== -->
	<div id="sidebar" class="span3">
		<div class="well well-small"><a id="myCart" href="{{route('shop.shop.cart')}}"><img src="{{ $publicUrl }}/themes/images/ico-cart.png" alt="cart">
		
			[ <b id="qtyCart"> {{ Cart::count() }} </b> ] Sản phẩm trong giỏ hàng!
		
		<span class="badge badge-warning pull-right" style="font-size:10px"></a></div>

		<ul id="sideManu" class="nav nav-tabs nav-stacked">
			@foreach($CatSidebar as $arCats)
				@php
					$pid = $arCats->parent_id;
					$Elec = DB::table('categories')->where('parent_id', '=', 1)->get();
					$Acce = DB::table('categories')->where('parent_id', '=', 2)->get();
					$Others = DB::table('categories')->where('parent_id', '=', 3)->get();
				@endphp
			@endforeach
			<li class="subMenu open"><a> Quần áo nam </a>
				<ul>
				@foreach($Elec as $arElec)
					@php
						$id_cat = $arElec->id;
                        $name = $arElec->name;
                        $name_slug = str_slug($name);
                        $urlCat = route('shop.shop.category',['slug' => $name_slug,'id' => $id_cat]);
					@endphp
				<li><a class="active" href="{{$urlCat}}"><i class="icon-chevron-right"></i>	
				 {{ $arElec->name }}
				 </a></li>	
				 @endforeach
				</ul>
			</li>
			<li class="subMenu"><a> Quần áo nữ </a>
			<ul style="display:block">
				@foreach($Acce as $arAcces)
					@php
						$id_cat = $arAcces->id;
                        $name = $arAcces->name;
                        $name_slug = str_slug($name);
                        $urlCat = route('shop.shop.category',['slug' => $name_slug,'id' => $id_cat]);
					@endphp
				<li><a href="{{$urlCat}}"><i class="icon-chevron-right"></i>{{$arAcces->name}} </a></li>
				@endforeach												
			</ul>
			</li>
			<li class="subMenu"><a> Giày </a>
			<ul style="display:block">
				@foreach($Others as $arOthers)
					@php
						$id_cat = $arOthers->id;
                        $name = $arOthers->name;
                        $name_slug = str_slug($name);
                        $urlCat = route('shop.shop.category',['slug' => $name_slug,'id' => $id_cat]);
					@endphp
				<li><a href="{{$urlCat}}"><i class="icon-chevron-right"></i>{{$arOthers->name}}</a></li>
				@endforeach												
			</ul>
			</li>
			
		</ul>
		<br/>
		<h5>Sản phẩm được đề xuất</h5>
		@php
			$Rem = DB::table('products')->where('trash','=',1)->where('promotion_price','<>',0)->orderBy('unit_price','DESC')->limit(3)->get();
		@endphp
		@foreach($Rem as $arRem)
		@php
    		$slug = str_slug($arRem->name);
			$url = route('shop.shop.detail',['slug'=>$slug,'id'=>$arRem->id]);
    	@endphp  
		<div class="thumbnail">
			<a href="{{$url}}"><h5>{{$arRem->name}}</h5></a>
			<img src="{{ $ImagesPath }}/{{$arRem->images}}" style="width: 200px" alt="Bootshop"/>
			<div class="caption">
				<h4 style="text-align:center">
				  <a href="javascript:;" id="{{$arRem->id}}" class="btn btn-warning addCart add-to-cart">Thêm vào <i class="icon-shopping-cart"></i></a>
				  	@if($arRem->promotion_price == 0)
				  		<a class="btn btn-primary" href="#">${{$arRem->unit_price}}</a>
				  	@else
				  		<a class="btn btn-danger" style="text-decoration: line-through;" href="#">{{$arRem->unit_price}}.000 VNĐ</a>
				  		<a class="btn btn-primary" href="#">{{$arRem->promotion_price}}.000 VNĐ</a>
				  	@endif
				</h4>
			</div>
		</div><br/>
		@endforeach
		<div class="thumbnail">
				<img src="{{ $publicUrl }}/themes/images/payment_methods.png" title="Bootshop Payment Methods" alt="Payments Methods">
				<div class="caption">
				  <h5>Hình thức thanh toán</h5>
			</div>
	</div>

	</div>

<!-- Sidebar end=============================================== -->