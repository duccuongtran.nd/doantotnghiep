<?php

namespace App\Http\Controllers\Admin;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    public function index(){
    	$objUsers = User::paginate(10);
    	return view('admin.user.index',compact('objUsers'));
    }
    public function getEdit($id){
    	$objUser = User::findOrFail($id);
      return view('admin.user.edit',compact('objUser'));
    }
    public function postEdit($id,Request $request){
        $objItems= User::find($id);
		$password = trim($request->password);
	    $oldpassword = trim($request->oldpassword);
	    $fullname = trim(stripslashes($request->fullname));
	    $email = trim(stripslashes($request->email));
	    $role = $request->role;
	    if ($password != "") {
	        if (Hash::check($oldpassword, $objItems->password)) {
	            $objItems->password = bcrypt($password);
	        }else{
	            $request->session()->flash('msg','Mật khẩu cũ không đúng');
	            return redirect()->route('admin.user.edit',$id); 
	        }
	    }else{
	        $password = $objItems->password;

	    }
	    $objItems->email = $email;
	    $objItems->fullname = $fullname;
	    $objItems->username = $objItems->username;
	    $objItems->role = $role;
	    $objItems->phone = $request->phone;
	    $objItems->address = $request->address;
	    if($objItems->update()){
	        $request->session()->flash('msg','Sửa thành công');
	        return redirect()->route('admin.user.index');
	    }else{
	        $request->session()->flash('msg','Đã xảy ra lỗi!');
	        return redirect()->route('admin.user.index');
	    }
    }
    public function del($id, Request $request){
        $objItem = User::FindOrFail($id);
        $username = $objItem->username;
        if ($username != 'admin') {
            if($objItem->delete()){
              $request->session()->flash('msg','Xóa thành công');
              return redirect()->route('admin.user.index');
              }else{
              $request->session()->flash('msg','Đã xảy ra lỗi!');
              return redirect()->route('admin.user.index');
            }
        }else{
	        $request->session()->flash('msg','Bạn không thể xóa user này !');
	        return redirect()->route('admin.user.index');
        }
    }
    
}
